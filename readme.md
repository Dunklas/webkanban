## Webkanban

WebKanban is a web application which let you create and share simple kanban/scrum boards.

## Get started: 
 - Clone the project
 - Create .env (see .env.example), add app key and pusher credentials
 - Deploy in some kind of web server...
