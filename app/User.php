<?php

namespace webkanban;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

/*
 * This class represents a registered user.
 */
class User extends Authenticatable {

  /* Table name in database. The variable is actually not needed, since Laravel implicitally uses model-name in plural. */
  protected $table = "users";

  /* These fields are hidden in any response to client, e.g. JSON. */
  protected $hidden = [
        'password', 'remember_token',
    ];

  /* This function defines a one-to-many relationship for this model. */
  public function ownedBoards() {
    return $this->hasMany('webkanban\Board', 'owner_id', 'id');
  }

  /* This function defines a many-to-many relationship for this model. */
  public function sharedBoards() {
    return $this->belongsToMany('webkanban\Board', 'board_user', 'user_id', 'board_id');
  }

  /* This function defines a one-to-many relationship for this model. */
  public function comments() {
    return $this->hasMany('webkanban\Comment', 'user_id', 'id');
  }

  /* This function returns a boolean indicating if this user is owner of $board. */
  public function isOwner($board) {
    $board = Board::find($board);
    $ownedBoards = $this->ownedBoards()->get();

    return $ownedBoards->contains($board);
  }
}
