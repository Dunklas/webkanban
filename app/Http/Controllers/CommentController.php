<?php

namespace webkanban\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use webkanban\Comment;
use webkanban\Events\CommentAdded;

/*
 * This controller class handles requests made to a
 * comment-related endpoint.
 */
class CommentController extends Controller {

    /* Creates a comment.
     * Returns HTTP 200. */
    public function create($board, $task, Request $request) {

      /* Validate input */
      $request->validate([
        'comment' => 'required|min:1|max:30',
      ]);

      /* Create comment and set attribute values */
      $comment = new Comment();
      $comment->message = $request->input('comment');
      $comment->task_id = $task;
      $comment->user_id = Auth::id();
      $comment->save();

      /* Broadcast this action */
      event(new CommentAdded(
        $comment->task->load('comments.author')->toJson(),
        $board));

      return response($comment->load('author'), 200);

    }
}
