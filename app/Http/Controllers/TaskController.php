<?php

namespace webkanban\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Auth;
use Validator;

use webkanban\Task;
use webkanban\Board;
use webkanban\Events\TaskCreated;
use webkanban\Events\TaskUpdated;
use webkanban\Events\TaskDeleted;

/*
 * This controller class handles requests made to a
 * task-related endpoint.
 */
class TaskController extends Controller {

    /* Creates a new task. Returns HTTP 200 if successful */
    public function create($board, Request $request) {
      /* Do validation. Returns to same view with error messages flashed to session if failed */
      $request->validate([
        'title' => 'required|min:1|max:50',
      ]);

      /* Create new task, set attributes and store in db */
      $task = new Task();
      $task->name = $request->input('title');
      $task->status = 'todo';
      $task->board_id = $board;
      $task->assigned_to = Auth::id();
      $task->save();

      /* Broadcast this action */
      event(new TaskCreated(
        $task->load('comments.author')->toJson(),
        $board));

      return response($task, 200);
    }

    /* Returns requested task, or HTTP 404 if task is not found. */
    public function fetch($board, $task) {
      return Task::findOrFail($task)->toJson();
    }

    /* Updates a given task. */
    public function update($board, $task, Request $request) {
      /* Find task and board. Returns HTTP 404 if not found. */
      $dbTask = Task::findOrFail($task);
      $dbBoard = Board::findOrFail($board);

      /* Extracts possible assignees for this task (owner and members of board) */
      $possibleAssignees = $dbBoard->members()->pluck('id')->merge(
        $dbBoard->owner()->pluck('id'))->toArray();

      /* Validate title, status and that assigned_to exists in $possibleAssignees
       * Returns to same view with error messages flashed to session if fail */
      Validator::make($request->all(), [
        'title' => [
          'required',
          'min:1',
          'max:50',
        ],
        'status' => [
          Rule::in(['todo', 'doing', 'done', 'archived'])
        ],
        'assigned_to' => [
          Rule::in($possibleAssignees)
        ]
      ], [
        'status.in' => 'Status must be todo, doing, done or archived',
        'assigned_to.in' => 'Assignee must be a member of this board'
      ])->validate();

      /* Create task, set attributes and store in db */
      $dbTask->name = $request->input('title');
      $dbTask->status = $request->input('status');
      $dbTask->assigned_to = $request->input('assigned_to');
      $dbTask->save();

      /* Broadcast this action */
      event(new TaskUpdated(
        $dbTask->load('comments.author')->toJson(),
        $board));

      return response($dbTask, 200);
    }

    /* Deletes specified task */
    public function destroy($board, $task) {
      /* Returns 404 if not found */
      $dbTask = Task::findOrFail($task);

      /* Broadcast this event */
      event(new TaskDeleted(
        $dbTask->load('comments.author')->toJson(),
        $board));

      $dbTask->delete();

    }
}
