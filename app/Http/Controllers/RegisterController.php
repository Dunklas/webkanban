<?php

namespace webkanban\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use webkanban\User;

/*
 * This controller class handles requests made to a
 * registration-related endpoint.
 */
class RegisterController extends Controller {

  /* Returns a view for the registration page */
  public function show() {
    return view('register');
  }

  /* Executed for registration requests */
  public function store(Request $request) {

    /* Do validation, if not successful, redirects to current page with error messages
     * flashed to session data. */
    $validatedData = $request->validate([
      'email' => 'required|email|unique:users',
      'password' => 'required|min:6',
      'confirm_password' => 'required|same:password',
    ]);

    /* Create new user object, set attributes (including hashed password) */
    $user = new User();
    $user->email = $request->input('email');
    $user->password = Hash::make(
      $request->input('password')
    );
    $user->save();

    /* Redirect to same view, with a success message flashed to session data */
    return redirect('register')->with('success', 'Account created successfully');
  }

}
