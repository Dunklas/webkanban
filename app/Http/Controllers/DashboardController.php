<?php

namespace webkanban\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;

/*
 * This controller class handles requests made to a
 * dashboard-related endpoint.
 */
class DashboardController extends Controller {

  /* Returns the dashboard view */
  public function show() {
    return view('dashboard');
  }

  /* Returns owned board in JSON format */
  public function owned() {
    return Auth::user()->ownedBoards->toJson();
  }

  /* Returns boards that an authenticated user has access to, in JSON format */
  public function shared() {
    return Auth::user()->sharedBoards->toJson();
  }

}
