<?php

namespace webkanban\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Auth;
use Validator;


use webkanban\User;
use webkanban\Board;
use webkanban\Events\BoardDeleted;
use webkanban\Events\BoardUpdated;

/*
 * This controller class handles requests made to a
 * board-related endpoint.
 */
class BoardController extends Controller {



    /* Only returns the actual view (blade/html template)
     * The board data is collected via ajax from other methods in this controller. */
    public function show($board) {
      return view('board', ['board' => $board]);
    }


    /* Creates a new board.
     * Returns HTTP 200. */
    public function create(Request $request) {

      $request->validate([
        'title' => 'required|min:1|max:20'
      ]);

      $board = new Board();
      $board->owner_id = Auth::id();
      $board->name = $request->input('title');
      $board->save();
      return response($board, 200);
    }


    /* Returns requested board in json format (including owner, member and tasks).
     * If not found, return HTTP 404. */
    public function fetch($board) {
      $dbBoard = Board::findOrFail($board);
      return $dbBoard->load('owner', 'members', 'tasks.comments.author')->toJson();
    }


    /* Deletes requested board.
     * Returns deleted board if successful, otherwise HTTP 404. */
    public function destroy($board) {
      $dbBoard = Board::findOrFail($board);
      /* Broadcast event to notify clients about this action. */
      event(new BoardDeleted($dbBoard->load('members')->toJson(), $dbBoard->id));
      $dbBoard->delete();
      return response($dbBoard, 200);
    }

    /*
     * Updates members or name for requested board.
     * Returns HTTP 404 if the board is not found.
     * Expects post data or JSON object. E.g:
     * {
     *   "attribute": "members",
     *   "operation": "add",
     *   "value": 2
     * }
     */
    public function update($board, Request $request) {

      $dbBoard = Board::findOrFail($board);
      $attribute = $request->input('attribute');
      $operation = $request->input('operation');
      $value = $request->input('value');

      /* This update is related to the members attribute */
      if ($attribute === 'members') {

        /* These validation rules are applicable to all members-related actions. */
        Validator::make($request->all(), [
          'value' => [
            'required',
            'email',
            'exists:users,email'
          ]
        ], [
          'value.required' => 'E-mail address is required.',
          'value.email' => 'The specified e-mail address is not valid',
          'value.exists' => 'The specified e-mail address is not a registered user.',
        ])->validate();

        /* Extract user object for the members to be added or removed. */
        $user = User::where('email', $value)->first();

        switch ($operation) {

          /* This update is for adding access for a user to a board. */
          case 'add':
            /* These validation rules are applicable to add operations only. */
            Validator::make($request->all(), [
              'value' => [
                Rule::notIn($dbBoard->members()->pluck('email')->toArray()),
                Rule::notIn($dbBoard->owner()->pluck('email')->toArray())
              ]
            ], [
              'value.not_in' => 'The specified e-mail address is already a member of this board.'
            ])->validate();

            /* Grant user access */
            $dbBoard->members()->attach($user);

            /* Broadcast this action */
            event(new BoardUpdated($dbBoard->load('members', 'owner')->toJson(), $dbBoard->id));

            return response($user, 200);
            break;

          /* This update is for removing access for a user to a board. */
          case 'remove':
            /* These validation rules are applicable to remove operations only. */
            Validator::make($request->all(), [
              'value' => [
                Rule::in($dbBoard->members()->pluck('email')->toArray()),
                Rule::notIn($dbBoard->owner()->pluck('email')->toArray())
              ]
            ], [
              'value.in' => 'The specified email address is not a member of this board.',
              'value.not_in' => 'The specified email address is owner of this board.'
            ])->validate();

            /* Remove user access */
            $dbBoard->members()->detach($user);

            /* Broadcast this action */
            event(new BoardUpdated($dbBoard->load('members', 'owner')->toJson(), $dbBoard->id));

            return response($user, 200);
            break;
        }
      } else if ($attribute === 'title') { /* This update is for modifying the title of a board */
          switch ($operation) {
            case 'update':

              Validator::make($request->all(), [
                'value' => 'required|min:1|max:20'
              ], [
                'value.required' => 'Title is required.',
                'value.min' => 'The title may not be less than 1 character.',
                'value.max' => 'The title may not be greater than 20 characters.'
              ])->validate();

              /* Update name of the board */
              $dbBoard->name = $value;
              $dbBoard->save();

              event(new BoardUpdated($dbBoard->load('members', 'owner')->toJson(), $dbBoard->id));

              return response($dbBoard, 200);
          }
      } else {
        /* No known arguments, return HTTP 400 */
        return response("Unvalid arguments", 400);
      }
    }
}
