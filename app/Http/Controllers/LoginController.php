<?php

namespace webkanban\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

/*
 * This controller class handles requests made to a
 * login-related endpoint.
 */
class LoginController extends Controller {

  /* Returns a view for a login page */
  public function show() {
    return view('login');
  }

  /* Executed for login attempts */
  public function login(Request $request) {
    /* Validate input */
    $validatedData = $request->validate([
      'email' => 'required|email|exists:users,email',
      'password' => 'required',
    ]);

    /* Auth::attempt uses the model defined in auth.php. I use the default name but have modified the actual model class. */
    $data = $request->except('_token');
    if (Auth::attempt($data)) {
      /* Redirect to root on successful attempt */
      return redirect('/');
    } else {
      /* Flash session data with error details */
      return redirect('login')->with('authFail', 'Incorrect password');
    }
  }

  /* Executed for logout requests */
  public function logout() {
    Auth::logout();
    return redirect('/');
  }
}
