<?php

namespace webkanban\Http\Middleware;

use Closure;
use Auth;
use webkanban\Board;

/*
 * This is a middleware class which handles incoming HTTP requests.
 * The handle()-method is called for each end point where this middleware
 * is applied. This one is used to confirm that a client is owner or member of
 * a requested board.
 */
class OwnerOrMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

      /* Extract boardId from url path */
      $boardId = $request->route('board');

      /* Will return a 404 HTTP response if not found */
      $board = Board::findOrFail($boardId);

      /* Extract the authenticated user */
      $user = Auth::user();

      /* Extract list of members of requested board */
      $members = $board->members()->get();

      /* Extract owner of requested board */
      $owner = $board->owner;

      // Will redirect to '/' if user is not member or owner of requested board
      if (!$members->contains($user) && $owner != $user) {
        return redirect()->to('/');
      }

      // Proceed with request
      return $next($request);
    }
}
