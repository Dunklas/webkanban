<?php

namespace webkanban;

use Illuminate\Database\Eloquent\Model;

/*
 * This class represents a task which is related to a Board.
 */
class Task extends Model {

  /* This function defines a many-to-one relationship for this model. */
  public function board() {
    return $this->belongsTo('webkanban\Board', 'board_id', 'id');
  }

  /* This function defines a many-to-one relationship for this model. */
  public function assignee() {
    return $this->belongsTo('webkanban\User', 'assigned_to', 'id');
  }

  /* This function defines a one-to-many relationship for this model. */
  public function comments() {
    return $this->hasMany('webkanban\Comment', 'task_id', 'id');
  }
}
