<?php

namespace webkanban\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'webkanban\Events\Event' => [
            'webkanban\Listeners\EventListener',
        ],
        'webkanban\Events\BoardUpdated' => [

        ],
        'webkanban\Events\BoardDeleted' => [

        ],
        'webkanban\Events\TaskCreated' => [

        ],
        'webkanban\Events\TaskDeleted' => [

        ],
        'webkanban\Events\TaskUpdated' => [

        ],
        'webkanban\Events\CommentAdded' => [

        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
