<?php

namespace webkanban\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Auth;

use webkanban\Task;

/*
 * This class represents an event which is broadcasted
 * when a new task has been created.
 */
class TaskCreated implements ShouldBroadcast {

    use SerializesModels;

    public $task;
    public $user;
    protected $boardId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($task, $boardId) {
        $this->task = $task;
        $this->boardId = $boardId;
        $this->user = Auth::user()->toJson();
    }

    /**
     * Get the channels the event should broadcast on.
     * This event is broadcastet to a private channel for this board.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn() {
        return new PrivateChannel('board.'.$this->boardId);
    }

    /**
     * Returns the name of this event.
     * Used in client-side javascript to extract data from this event.
     */
    public function broadcastAs() {
      return 'board.task.created';
    }
}
