<?php

namespace webkanban;

use Illuminate\Database\Eloquent\Model;

/*
 * This class represents a kanban board.
 * It has relations to User (owner() and members()) and Task (tasks()).
 */
class Board extends Model {

     /* This function defines a many-to-many relation for this model. */
     public function members() {
       return $this->belongsToMany('webkanban\User', 'board_user', 'board_id', 'user_id');
     }

     /* This function defines a many-to-one relation for this model. */
     public function owner() {
       return $this->belongsTo('webkanban\User', 'owner_id');
     }

     /* This function defines a one-to-many relation for this model. */
     public function tasks() {
       return $this->hasMany('webkanban\Task', 'board_id');
     }
}
