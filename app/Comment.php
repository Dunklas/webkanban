<?php

namespace webkanban;

use Illuminate\Database\Eloquent\Model;

/*
 * This class represents a comment which can be made in a Task
 * which belongs to a board.
 */
class Comment extends Model {

    /* This function defines a many-to-one relationship for this model. */
    public function author() {
      return $this->belongsTo('webkanban\User', 'user_id');
    }

    /* This function defines a many-to-one relationship for this model. */
    public function task() {
      return $this->belongsTo('webkanban\Task', 'task_id');
    }
}
