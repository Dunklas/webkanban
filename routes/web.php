<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@show')->middleware("auth");

Route::get('/login', 'LoginController@show')->name('login');
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout')->middleware("auth");

Route::get('/register', 'RegisterController@show');
Route::post('/register', 'RegisterController@store');

/* Most likely this should be one method in BoardController, and then sort if owner or shared client side */
Route::get('/owned_boards', 'DashboardController@owned')->middleware("auth");
Route::get('/shared_boards', 'DashboardController@shared')->middleware("auth");

Route::get('/viewboard/{board}', 'BoardController@show')->middleware("auth", "owner.member");

Route::post('/board', 'BoardController@create')->middleware("auth");
Route::patch('/board/{board}', 'BoardController@update')->middleware("auth", "owner");
Route::get('/board/{board}', 'BoardController@fetch')->middleware("auth", "owner.member");
Route::delete('/board/{board}', 'BoardController@destroy')->middleware("auth", "owner");

/* Perhaps ALL POST should return the created object?? See: https://stackoverflow.com/questions/19199872/best-practice-for-restful-post-response */

Route::post('/board/{board}/task', 'TaskController@create')->middleware("auth", "owner.member");
Route::get('/board/{board}/task/{task}', 'TaskController@fetch')->middleware("auth", "owner.member");
Route::put('/board/{board}/task/{task}', 'TaskController@update')->middleware("auth", "owner.member");
Route::delete('/board/{board}/task/{task}', 'TaskController@destroy')->middleware("auth", "owner.member");

Route::post('/board/{board}/task/{task}/comment', 'CommentController@create')->middleware("auth", "owner.member");

Route::get('/whoami', function() {
  return Auth::user();
})->middleware("auth");
