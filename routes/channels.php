<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

use webkanban\Board;

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('board.{boardId}', function ($user, $boardId) {

  $board = Board::find($boardId);
  if ($board == null) {
    return false;
  }

  $members = $board->members()->get();
  $owner = $board->owner;

  if (!$members->contains($user) && $owner != $user) {
    return false;
  } else {
    return true;
  }

});
