/* Uses module pattern to encapsulate logic related to CreateBoard-dialogue */
var CreateBoard = (function () {

  /* Callback functions for success/failure when creating a board */
  var _onSuccess = null;
  var _onFailure = null;

  /* Setup callback methods and call _setup() to initialize handlers for click events */
  init = function(onSuccess, onFailure) {
    _onSuccess = onSuccess;
    _onFailure = onFailure;
    _setup();
  }

  /* Show this dialogue */
  show = function() {
    $('#createBoard').modal('show');
  }

  /* Setup handlers for click events */
  var _setup = function() {
    /* Focus on first text field on open */
    $('#createBoard').on('shown.bs.modal', function() {
      $('#newBoardTitle').focus();
    });

    /* When modal is hidden, clear HTML-field(s) and hide validation errors */
    $('#createBoard').on('hidden.bs.modal', function() {
      $('#newBoardTitle').val("");
      _hideValidationErrors();
    });

    /* When create button is clicked, hide validation errors, clear HTML-field(s) and invoke _clearBoard(title) */
    $('#createBoardBtn').on('click', function() {
      _hideValidationErrors();

      var title = $('#newBoardTitle').val();
      _createBoard(title);
    });
  }

  /* Sends POST request to create a board */
  var _createBoard = function(title) {
      $.ajax({
        url: '/board',
        type: 'POST',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data: {'title': title},
        success: function(response) {
          /* Successful. Hide modal and invoke callback function. */
          $('#createBoard').modal('hide');
          if (_onSuccess != null) {
            _onSuccess();
          }
        },
        error: function(response) {
          /* If validation errors, show them */
          if (response.responseJSON.errors) {
            _showValidationErrors(response.responseJSON);
          } else {
            /* Other failure, hide modal and invoke callback function. */
            if (_onFailure != null) {
              _onFailure();
            }
            $('#createBoard').modal('hide');
          }
        }
      });
  }

  var _showValidationErrors = function(response) {
    if (response.errors.title) {
      $('#newBoardTitle').parent().addClass('has-error');
      $('#newBoardTitle').parent().append(
        '<span class="help-block">' + response.errors.title + '</span>'
      );
    }
  }

  var _hideValidationErrors = function() {
    $('#createBoardForm .has-error').removeClass('has-error');
    $('#createBoardForm .help-block').remove();
  }

  return {
    init: init,
    show: show
  }

})();

/* Uses module pattern to encapsulate logic related to Error-dialogue */
var Error = (function () {

  var _message = null;

  /* Set message for this dialogue */
  setMessage = function(message) {
    _message = message;
    $('#errorMsg').html(_message);
  }

  /* Show dialogue */
  show = function() {
    $('#error').modal('show');
  }

  return {
    setMessage: setMessage,
    show: show
  }

})();

/* Variable where boards owned by current user are stored */
var myBoards;

/* Variable where boards shared with current user are stored */
var sharedBoards;

$("#document").ready(function() {
  registerHandlers();
  resetBoardCards();

  /* Initialize CreateBoard-dialogue with callback functions */
  CreateBoard.init(
    function() {
      // Board created successfully, re-render this view.
      resetBoardCards();
    },
    function() {
      // Error occurred while creating board
      Error.setMessage('An error occurred while creating your board. Please try again.');
      Error.show();
    }
  )

  /* Check every 5s if there is new data (shared boards). */
  setInterval(function() {
    resetBoardCards();
  }, 5000);
});

/* Fires getMyBoards and getSharedBoards, compare result with previous data.
 * If not equals, re-create views. */
function resetBoardCards() {
  getMyBoards(function(response) {
    /* Only re-render view if we find a difference between response and myBoards */
    if (response != myBoards) {
      myBoards = response;
      /* Skip first div, since this one is used as a "create button" */
      $('#myBoardsContainer').children().not('div:first').remove();
      renderBoard('#myBoardsContainer', JSON.parse(myBoards));
    }
  });
  getSharedBoards(function(response) {
    /* Only re-render view if we find a difference between response and sharedBoards */
    if (response != sharedBoards) {
      sharedBoards = response;
      $('#sharedBoardsContainer').empty();
      renderBoard('#sharedBoardsContainer', JSON.parse(sharedBoards));
    }
  });
}

/* Fetches boards owned by current user (GET) */
function getMyBoards(onSuccess) {
  var response = $.get("/owned_boards", function(response) {
    onSuccess(response);
  }).fail(function() {
    Error.setMessage('An error occurred while loading your boards. Please try again later.');
    Error.show();
  });
}

/* Fetches boards shared with current user (GET) */
function getSharedBoards(onSuccess) {
  var response = $.get("/shared_boards", function(response) {
    onSuccess(response);
  }).fail(function() {
    Error.setMessage('An error occurred while loading your boards. Please try again later.');
    Error.show();
  });
}

/* Renders board data in this view.
 * Hidden input element is used to store id of each board.
 * The id is used to build urls in order to open the boards */
function renderBoard(parent, data) {
  var boardHTML = "";
  $.each(data, function(index, obj) {
    boardHTML += '<div class="card card-postit board-link">';
    boardHTML += '<input type="hidden" value="' + obj.id + '">';
    boardHTML += '<span class="card-title">' + obj.name + '</span>';
    boardHTML += '</div>';
  });
  $(parent).append(boardHTML);
}

/* Setup handlers for click events */
function registerHandlers() {
  /* When create-board-card is clicked, open modal. */
  $('#createBoardCard').on('click', function() {
    CreateBoard.show();
  });

  /* Handler for all other cards (except the create-board-one). Uses event delegation
   * since the elements (.board-card) doesn't exist until fetched from server.
   * Each element has a hidden input field with a board-id as first element, which is used
   * to get an id for the link. */
  $('.card-container').on('click', '.board-link', function() {
    var id = $(this).find('>:first-child').val();
    window.location.href = 'viewboard/' + id;
  });
}
