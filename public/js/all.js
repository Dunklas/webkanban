/* Returns a string (YYY-MM-DD HH:MM:SS) in local time (from UTC) */
function toLocalDateTimeString(datetime) {

  var datetimeParts = datetime.split(' ');
  var dateArr = datetimeParts[0];
  var timeArr = datetimeParts[1];

  var dateParts = datetimeParts[0].split('-');
  var timeParts = datetimeParts[1].split(':');

  var dtObj = new Date(Date.UTC(dateParts[0], dateParts[1]-1, dateParts[2], timeParts[0], timeParts[1], timeParts[2]));

  var year = dtObj.getFullYear();
  var month = dtObj.getMonth() + 1;
  var day = dtObj.getDate();
  var hour = dtObj.getHours();
  var min = dtObj.getMinutes();
  var sec = dtObj.getSeconds();

  month = (month < 10) ? '0' + month : month;
  day = (day < 10) ? '0' + day : day;
  hour = (hour < 10) ? '0' + hour : hour;
  min = (min < 10) ? '0' + min : min;
  sec = (sec < 10) ? '0' + sec : sec;

  return year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec;
}

var Auth = (function () {

  /* This function returns a JSON representation of the authenticated user */
   currentUser = function(onSuccess) {
     $.ajax({
       url: '/whoami/',
       type: 'GET',
       success: function(response) {
         onSuccess(response);
       },
       error: function(response) {
         return false;
       }
     });
   }

   return {
     currentUser: currentUser
   }

}());
