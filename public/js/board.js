/* Uses module pattern to encapsulate logic related to ModifyTask-dialogue */
var ModifyTask = (function () {

  var _isOpen = false;

  var _boardId = null;
  var _boardOwner = null;
  var _members = null;

  var _taskId = null;
  var _taskTitle = null;
  var _taskStatus = null;
  var _taskAssignee = null;
  var _taskComments = null;

  var _onTaskUpdateFailure = null;
  var _onTaskDeleteFailure = null;
  var _onCommentAddFailure = null;

  /* Setup callback functions and invoke _setup() */
  init = function (onTaskUpdateFailure, onTaskDeleteFailure, onCommentAddFailure) {
    _onTaskUpdateFailure = onTaskUpdateFailure;
    _onTaskDeleteFailure = onTaskDeleteFailure;
    _onCommentAddFailure = onCommentAddFailure;
    _setup();
  };

  /* Show dialogue */
  show = function() {
    _isOpen = true;
    $('#modifyTask').modal('show');
  }

  /* Hide dialogue */
  hide = function() {
    $('#modifyTask').modal('hide');
  }

  /* Returns a boolean indicating if this dialogue is open or not */
  isOpen = function() {
    return _isOpen;
  }


  /* Returns the id of the task currently displayed in this dialogue. */
  currentTask = function() {
    return _taskId;
  }


  /* Updates this modal with new state.
   * This is needed when any property of a board, or members
   * of a board has changed. These properties are the same,
   * regardless of which task is open in this dialogue. */
  update = function(boardId, boardOwner, members) {
    _boardId = boardId;
    _boardOwner = boardOwner;
    _members = members;

    _renderCommon();
  }


  /* Updates this modal with state of a specific task.
   * This is normally done right before this dialogue is opened.
   * However, it may be updated while open if another user has
   * made an update to the task currently open in this dialogue. */
  setTask = function(task) {
    _taskId = task.id;
    _taskTitle = task.name;
    _taskStatus = task.status;
    _taskAssignee  = task.assigned_to;
    _renderTask();
  }


  /* Updates this modal with comments of a specific task.
   * This could be included in setTask(task), but I separated
   * them in order to be able to update task data and comments separately.
   * Otherwise, the following scenario could happen:
   * 1. User writes a comment but do NOT click add-button.
   * 2. User changes status of task.
   * 3. User click add-button.
   * 4. Status is reverted back to previous value when comment is added. */
  setComments = function(comments) {
    _taskComments = comments;
    _renderComments();
  }


  /* Setup event handlers for click events, etc used in this dialogue. */
  var _setup = function() {
    /* Hide validation errors when modal is hidden. */
    $('#modifyTask').on('hidden.bs.modal', function() {
      _isOpen = false;
      _hideValidationErrors();
    });

    /* Invoke _updateTask when user click save button. */
    $('#taskModify').on('click', function() {
      _hideValidationErrors();
      var title = $('#taskTitle').val();
      var status = $('#taskStatus').val();
      var assignee = $('#taskAssignee').val();
      _updateTask(title, status, assignee);
    });

    /* Invoke _addComment when user click add comment button. */
    $('#taskAddComment').on('click', function() {
      _hideValidationErrors();
      var message = $('#taskComment').val();
      _addComment(message);
    });

    /* Invoke _deleteTask when user click delete button. */
    $('#taskDelete').on('click', function() {
      _deleteTask();
    });
  }


  /* Render properties which are common for this dialogue,
   * regardless of which task is opened. */
  var _renderCommon = function() {
    $('#taskAssignee').html('');

    var memberHTML;
    memberHTML += _getUserOption(_boardOwner);
    for (var [id, member] of _members) {
      memberHTML += _getUserOption(member);
    }
    $('#taskAssignee').append(memberHTML);

    if (_taskAssignee != null) {
      $('#taskAssignee').val(_taskAssignee);
    }
  }

  /* Returns an <option>-element for provided member. */
  var _getUserOption = function(member) {
    var html = '<option value="' + member.id + '"';
    html += '>' + member.email + '</option>'
    return html;
  }


  /* Render properties which are specific to the
   * task which should be displayed in this dialogue. */
  var _renderTask = function() {
    /* Set title */
    $('#taskHeader').html(_taskTitle);
    $('#taskTitle').val(_taskTitle);

    /* Set status */
    $('#taskStatus').val(_taskStatus);

    /* Set selected member to the task assignee */
    $('#taskAssignee').val(_taskAssignee);
  }


  /* Render comments which are specific to
   * the task displayed in this dialogue. */
  var _renderComments = function() {
    $('#taskLog').html('');
    $.each(_taskComments, function(i, comment) {
      _renderComment(comment);
    });
  }

  /* Renders HTML elements for one comment */
  var _renderComment = function(comment) {
    var commentHTML = '<tr><td>';
    commentHTML += comment.author.email + '</td><td>';
    commentHTML += toLocalDateTimeString(comment.created_at) + '</td><td>';
    commentHTML += comment.message + '</td></tr>'
    $('#taskLog').prepend(commentHTML);
  }


  /* Sends a HTTP PUT request in order to update this task. */
  var _updateTask = function(title, status, assignee) {

    $.ajax({
      url: '/board/' + _boardId + '/task/' + _taskId,
      type: 'PUT',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        'title': title,
        'status' : status,
        'assigned_to' : assignee
      },
      success: function(response) {
        $('#modifyTask').modal('hide');
      },
      error: function(response) {
        /* Validation errors, show them */
        if (response.responseJSON.errors) {
          _showValidationErrors(response.responseJSON);
        } else {
          /* Other error, invoke callback function */
          $('#modifyTask').modal('hide');
          _onTaskUpdateFailure();
        }
      }
    });
  }


  /* Sends a HTTP DELETE request in order to delete this task. */
  var _deleteTask = function() {
    $.ajax({
      url: '/board/' + _boardId + '/task/' + _taskId,
      type: 'DELETE',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      success: function(response) {
        $('#modifyTask').modal('hide');
      },
      error: function() {
        /* Invoke callback function */
        $('#modifyTask').modal('hide');
        _onTaskDeleteFailure();
      }
    });
  }


  /* Sends a HTTP POST request to add a comment to this task. */
  var _addComment =  function(message) {
    $.ajax({
      url: '/board/' + _boardId + '/task/' + _taskId + '/comment',
      type: 'POST',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        'comment': message
      },
      success: function(response) {
        $('#taskComment').val('');
      },
      error: function(response) {
        /* Validation errors, show them! */
        if (response.responseJSON.errors) {
          _showValidationErrors(response.responseJSON);
        } else {
          /* Invoke callback function */
          $('#taskComment').val('');
          _onCommentAddFailure();
        }
      }
    });
  }


  /* Shows validation errors in this dialogue.
   * Server sends a JSON response with validation errors,
   * which is expected as parameter to this function. */
  var _showValidationErrors = function(response) {
    if (response.errors.title) {
      $('#taskTitle').parent().addClass('has-error');
      $('#taskTitle').parent().append(
        '<span class="help-block">' + response.errors.title + '</span>'
      );
    } else if (response.errors.comment) {
      $('#taskComment').parent().parent().addClass('has-error');
      $('#taskComment').parent().parent().append(
        '<span class="help-block">' + response.errors.comment + '</span>'
      )
    }
  }


  /* Removes all validation errors. */
  var _hideValidationErrors = function(response) {
    $('#modifyTaskForm .has-error').removeClass('has-error');
    $('#modifyTaskForm .help-block').remove();
  }

  /* Returns publically exposed methods. */
  return {
    init: init,
    update: update,
    show: show,
    hide: hide,
    isOpen : isOpen,
    currentTask : currentTask,
    setTask : setTask,
    setComments : setComments
  };

})();

/* Uses module pattern to encapsulate logic related to CreateTask-dialogue */
var CreateTask = (function () {

  var _boardId = null;
  var _onTaskCreateFailure = null;

  /* Update this modal with a board id (needed for path building for HTTP requests) */
  update = function(boardId) {
    _boardId = boardId;
  }

  /* Show dialogue */
  show = function() {
    $('#createTask').modal('show');
  }

  /* Setup callback function and invoke _setup() */
  init = function(onTaskCreateFailure) {
    _onTaskCreateFailure = onTaskCreateFailure;
    _setup();
  }

  /* Setup handlers for click events */
  var _setup = function() {
    /* Set focus on first field when modal is displayed. */
    $('#createTask').on('shown.bs.modal', function() {
      $('#newTaskTitle').focus();
    });
    /* Clear HTML field when modal is hidden. */
    $('#createTask').on('hidden.bs.modal', function() {
      $('#newTaskTitle').val("");
      _hideValidationErrors();
    });
    /* Invoke _createTask when user click save button. */
    $('#taskSave').on('click', function() {
      _hideValidationErrors();

      var task_name = $('#newTaskTitle').val();
      _createTask(task_name);
    });
  }

  /* Send POST request to create a task */
  var _createTask = function(new_task_title) {
    $.ajax({
      url: '/board/' + _boardId + '/task',
      type: 'POST',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {'title': new_task_title},
      success: function(response) {
        /* Successful, hide modal */
        $('#createTask').modal('hide');
      },
      error: function(response) {
        /* Validation errors, show them! */
        if (response.responseJSON.errors) {
          _showValidationErrors(response.responseJSON);
        } else {
          /* Other error, hide dialogue and invoke callback function */
          $('#createTask').modal('hide');
          _onTaskCreateFailure();
        }
      }
    });
  }

  var _showValidationErrors = function(response) {
    if (response.errors.title) {
      $('#newTaskTitle').parent().addClass('has-error');
      $('#newTaskTitle').parent().append(
        '<span class="help-block">' + response.errors.title + '</span>'
      );
    }
  }

  var _hideValidationErrors = function() {
    $('#createTaskForm .has-error').removeClass('has-error');
    $('#createTaskForm .help-block').remove();
  }

  return {
    init: init,
    show: show,
    update: update
  }

})();

/* Uses module pattern to encapsulate logic related to ModifyMembers-dialogue */
var ModifyMembers = (function () {

  var _boardId;
  var _boardOwner;
  var _members;

  var _onAddMemberFailure = null;
  var _onRemoveMemberFailure = null;

  /* HTML elements to be used to render owner and each member of this board.
   * The elements are generated differently by server depending on if
   * current user is owner of the board or not. Therefore we need a template. */
  var _listTemplate = null;

  /* Update with new data and re-render dialogue */
  update = function(boardId, boardOwner, members) {
    _boardId = boardId;
    _boardOwner = boardOwner;
    _members = members;
    _renderDialogue();
  }

  /* Show dialogue */
  show = function() {
    $('#modifyMembers').modal('show');
  }

  /* Setup callback functions and invoke _setup() */
  init = function(onAddMemberFailure, onRemoveMemberFailure) {
    _onAddMemberFailure = onAddMemberFailure;
    _onRemoveMemberFailure = onRemoveMemberFailure;
    _setup();
  }

  /* Setup handlers for click events, etc */
  var _setup = function() {
    /* This template is used for each member of this board.
     * The template looks different, depending on if the user is owner of the board or not. */
    _listTemplate = $('.memberTemplate').first().clone();
    $('.memberTemplate').remove();

    $('#memberAdd').on('click', function() {
      _hideValidationErrors();

      var email = $('#memberEmail').val();
      _addMember(email);
    });

    /* Uses event delegation, since these li elements are not present initially
     * The button invoking this function is used to remove a member in this board. */
    $('#memberList').on('click', '.memberRemove', function() {
      /* Extract user id */
      var user_email = $(this).closest('li').contents().get(0).nodeValue;
      _removeMember(user_email);
    });

    $('#modifyMembers').on('hidden.bs.modal', function() {
      $('#memberEmail').val('');
      _hideValidationErrors();
    });
  }

  /* Render HTML elements based on data provided in update()-function */
  var _renderDialogue = function() {
    /* Clear member list */
    $('#memberOwner').html('');
    $('#memberList li:not(:first-child)').remove();

    /* Set owner list item */
    $('#memberOwner').html('<strong>' + _boardOwner.email + '</strong>');

    /* Render each member */
    for (var [id, member] of _members) {
      _renderMember(member);
    }

  }

  /* Invoked to render HTML elements for one members */
  var _renderMember = function(member) {
    /* Clone template and prepend email of member */
    var memberElement = _listTemplate.clone().find('>:first-child')
      .prepend(member.email);
    $('#memberList').append(memberElement);
  }

  var _unrenderMember = function(member) {
    $('#memberList li:contains("' + member.email + '")').remove();
  }

  /* Sends a PATCH request to add a member to this board */
  var _addMember = function(user_email) {
    $.ajax({
      url: '/board/' + _boardId,
      type: 'PATCH',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        'attribute': 'members',
        'operation' : 'add',
        'value' : user_email
      },
      success: function(response) {
        /* Do nothing, since events broadcasting (websockets/pusher) to clients take
           care of any rendering */
      },
      error: function(response) {
        /* Validation errors, show them! */
        if (response.responseJSON.errors) {
          _showValidationErrors(response.responseJSON);
        } else {
          /* Invoke callback function */
          _onAddMemberFailure();
        }
      }
    });
  }

  /* Sends a PATCH request to remove a member from this board */
  var _removeMember = function(user_email) {
    $.ajax({
      url: '/board/' + _boardId,
      type: 'PATCH',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        'attribute' : 'members',
        'operation' : 'remove',
        'value' : user_email
      },
      success: function(response) {
        /* Do nothing, since events broadcasting (websockets/pusher) to clients take
           care of any rendering */
      },
      error: function(response) {
        /* Invoke callback function */
        _onRemoveMemberFailure();
      }
    });
  }

  var _showValidationErrors = function(response) {
    if (response.errors.value) {
      $('#memberEmail').parent().parent().addClass('has-error');
      $('#memberEmail').parent().parent().append(
        '<span class="help-block">' + response.errors.value + '</span>'
      );
    }
  }

  var _hideValidationErrors = function() {
    $('#modifyMembersForm .has-error').removeClass('has-error');
    $('#modifyMembersForm .help-block').remove();
  }

  return {
    init: init,
    show: show,
    update: update
  }
})();

/* Uses module pattern to encapsulate logic related to ModifyBoard-dialogue */
var ModifyBoard = (function () {

  var _boardId = null;
  var _boardTitle = null;
  var _boardCreatedAt = null;

  var _onBoardModifyFailure = null;
  var _onBoardDeleteFailure = null;

  /* Setup callback functions and invoke _setup() */
  init = function(onBoardModifyFailure, onBoardDeleteFailure) {
    _onBoardModifyFailure = onBoardModifyFailure;
    _onBoardDeleteFailure = onBoardDeleteFailure;
    _setup();
  };

  /* Update dialogue with new data about this board, and re-render dialogue */
  update = function(boardId, boardTitle, boardCreatedAt) {
    _boardId = boardId;
    _boardTitle = boardTitle;
    _boardCreatedAt = boardCreatedAt;
    _renderDialogue();
  };

  /* Show dialogue */
  show = function() {
    $('#modifyBoard').modal('show');
  };

  /* Setup handlers for click events */
  var _setup = function() {
    /* Hide validation errors when modal is hidden */
    $('#modifyBoard').on('hidden.bs.modal', function() {
      _hideValidationErrors();
    });

    $('#boardDelete').on('click', function() {
      _deleteBoard();
    });

    /* Hide validation errors when save-button is clicked */
    $('#boardSave').on('click', function() {
      _hideValidationErrors();

      var newTitle = $('#boardTitle').val();
      _modifyBoard(newTitle);
    });
  }

  /* Render data about this board */
  var _renderDialogue = function() {
    $('#boardTitle').val(_boardTitle);
    $('#boardCreated').html(toLocalDateTimeString(_boardCreatedAt));
  }

  /* Sends a PATCH request to update this board */
  var _modifyBoard = function(title) {
    $.ajax({
      url: '/board/' + _boardId,
      type: 'PATCH',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        'attribute': 'title',
        'operation' : 'update',
        'value' : title
      },
      success: function(response) {
        /* Success, hide this modal */
        $('#modifyBoard').modal('hide');
      },
      error: function(response) {
        /* Validation errors, show them */
        if (response.responseJSON.errors) {
          _showValidationErrors(response.responseJSON);
        } else {
          /* Other error, hide modal and invoke callback function */
          $('#modifyBoard').modal('hide');
          _onBoardModifyFailure();
        }
      }
    });
  }

  /* Sends a DELETE request to delete this board */
  var _deleteBoard = function() {
    $.ajax({
      url: '/board/' + _boardId,
      type: 'DELETE',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      success: function(response) {
        /* Successful, hide modal */
        $('#modifyBoard').modal('hide');
      },
      error: function() {
        /* Failure, hide modal and invoke callback function */
        $('#modifyBoard').modal('hide');
        _onBoardDeleteFailure();
      }
    });
  }

  var _showValidationErrors = function(response) {
    if (response.errors.value) {
      $('#boardTitle').parent().addClass('has-error');
      $('#boardTitle').parent().append(
        '<span class="help-block">' + response.errors.value + '</span>'
      );
    }
  }

  var _hideValidationErrors = function() {
    $('#modifyBoardForm .has-error').removeClass('has-error');
    $('#modifyBoardForm .help-block').remove();
  }

  return {
    init: init,
    update: update,
    show: show
  }

})();

/* Uses module pattern to encapsulate logic related to ArchivedTasks-dialogue */
var ArchivedTasks = (function () {

  /* Re-renders this dialogue with new data */
  update = function(tasks) {
    $('#archivedTasksTbody').html('');
    for (var [id, task] of tasks) {
      /* Only render tasks with status 'archived' */
      if (task.status == 'archived') {
        _renderTask(task);
      }
    }
  }

  /* Show dialogue */
  show = function() {
    $('#archivedTasks').modal('show');
  }

  /* Render HTML-elements for one task */
  var _renderTask = function(task) {
    var taskHTML = '<tr><td>';
    taskHTML += task.name + '</td><td>';
    taskHTML += toLocalDateTimeString(task.updated_at) + '</td></tr>';
    $('#archivedTasksTbody').prepend(taskHTML);
  }

  return {
    update: update,
    show: show
  }

})();

/* Uses module pattern to encapsulate logic related to Error-dialogue */
var Error = (function () {

  var _message = null;

  /* Set message for this dialogue */
  setMessage = function(message) {
    _message = message;
    $('#errorMsg').html(_message);
  }

  /* Show dialogue */
  show = function() {
    $('#error').modal('show');
  }

  return {
    setMessage: setMessage,
    show: show
  }

})();

/* This js-file is used in view(s) where a board is displayed. */

$("#document").ready(function() {

  /* Extract id from url path */
  var pathArr = location.pathname.split('/');
  var board_id = pathArr[pathArr.length-1];

  Board.init(board_id);

});

/* Uses module pattern to encapsulate logic related to board-view. */
var Board = (function () {

  var _currentUser = null;

  var _boardId = null;
  var _boardTitle = null;
  var _boardOwner = null;
  var _boardCreatedAt = null;
  var _members = null;
  var _tasks = null;

  /* After a board has been fetched (GET), methods are called to setup this page/view */
  init = function(board_id) {
    _loadBoard(board_id, function() {
      // On successfully loaded board
      _renderBoard();
      _setupHandlers();
      _setupModals();
      _setupEventListeners();

      /* Store current user in order to enable comparison to check if events originates from this user. */
      Auth.currentUser(function(user) {
        _currentUser = user;
      });
    });
  }

  /* Load a board with a GET request. Store data from JSON response in variables with a leading underscore */
  var _loadBoard = function(board_id, onSuccess) {
    $.get("/board/" + board_id, function(response) {
      rJson = JSON.parse(response);

      _boardId = rJson.id;
      _boardTitle = rJson.name;
      _boardOwner = rJson.owner;
      _boardCreatedAt = rJson.created_at;

      _loadMembers(rJson.members);
      _loadTasks(rJson.tasks);

      onSuccess();
    }).fail(function() {
      Error.setMessage('An error occurred while loading the board. Please try again later.');
      Error.show();
    });
  }

  /* Store members in a map object */
  var _loadMembers = function(members) {
    _members = new Map();
    $.each(members, function(i, member) {
      _members.set(member.id, member);
    });
  }

  /* Store tasks in a map object */
  var _loadTasks = function(tasks) {
    _tasks = new Map();
    $.each(tasks, function(i, task) {
      _tasks.set(task.id, task);
    });
  }

  /* Called to render a board on the page */
  var _renderBoard = function() {
    _renderHeader();
    $('.card-container').html('');
    for (var [id, task] of _tasks) {
      if (task.status != 'archived') {
        _renderTask(task);
      }
    }
  }

  /* Show header */
  var _renderHeader = function() {
    $('#boardHeader').html(_boardTitle);
  }

  /* Show each task in a div element. Each tasks' id is inserted to a hidden input element.
   * The id is needed to build url paths to be used if user clicks on a task */
  var _renderTask = function(task) {
    var taskHTML = "";
    taskHTML += '<div class="card card-postit task">';
    taskHTML += '<input type="hidden" value="' + task.id + '">';
    taskHTML += '<span class="card-title">' + task.name + '</span>';
    taskHTML += '</div>';

    /* Different containers depending on status of task */
    switch (task.status) {
      case 'todo':
        $('#todoContainer').append(taskHTML);
        break;
      case 'doing':
        $('#doingContainer').append(taskHTML);
        break;
      case 'done':
        $('#doneContainer').append(taskHTML);
        break;
    }
  }

  /* Removes a div element representing a task */
  var _unrenderTask = function(task) {
    $('.card-container :input[value="' + task.id + '"]').parent().remove();
  }

  /* Setup handlers for click events */
  var _setupHandlers = function() {
    /* When create task-link is clicked, show modal. */
    $('#createTaskLink').on('click', function(evt) {
      evt.preventDefault();
      CreateTask.show();
    });

    /* When members-link is clicked, show modal. */
    $('#modifyMembersLink').on('click', function(evt) {
      evt.preventDefault();
      ModifyMembers.show();
    });

    /* When properties-link is clicked, show modal. */
    $('#modifyBoardLink').on('click', function(evt) {
      evt.preventDefault();
      ModifyBoard.show();
    });

    /* When archived tasks-link is clicked, show modal. */
    $('#archivedTasksLink').on('click', function(evt) {
      evt.preventDefault();
      ArchivedTasks.show();
    });

    /* When a task is clicked, show modal. Uses event delegation since the task
     * elements are created after document is loaded. */
    $('.card-container').on('click', '.task', function() {
      var taskId = Number($(this).find('>:first-child').val());
      ModifyTask.setTask(_tasks.get(taskId));
      ModifyTask.setComments(_tasks.get(taskId).comments);
      ModifyTask.show();
    });
  }

  /* Setup listeners using Laravel Echo. Listens to events broadcasted from backend
   * via Pusher. */
  var _setupEventListeners = function() {
    Echo.private('board.' + _boardId)

          /* New task created, add to _tasks and re-render the board */
          .listen('.board.task.created', (e) => {
            var newTask = JSON.parse(e.task);
            _tasks.set(newTask.id, newTask);
            _renderBoard();
          })

          /* Task has been updated. Update object in _tasks and re-render the board
           * If Modify task-dialogue is open, update data in that modal
           * Update ArchivedTasks-dialogue in case a task has been archived */
          .listen('.board.task.updated', (e) => {
            var updatedTask = JSON.parse(e.task);
            _tasks.set(updatedTask.id, updatedTask);

            if (ModifyTask.isOpen() &&
                 ModifyTask.currentTask() == updatedTask.id) {
                   ModifyTask.setTask(updatedTask);
            }

            ArchivedTasks.update(_tasks);

            _renderBoard();
          })

          /* Task has been deleted. Delete object from _tasks and re-render board.
           * If the user who removed the board is not the current user, show an error message */
          .listen('.board.task.deleted', (e) => {
            var deletedTask = JSON.parse(e.task);
            _tasks.delete(deletedTask.id);

            var changer = JSON.parse(e.user);

            if (ModifyTask.isOpen() &&
                 ModifyTask.currentTask() == deletedTask.id &&
                   changer.id != _currentUser.id) {
                   Error.setMessage('The task you are currently viewing or modifying has been deleted by ' + changer.email + '.');
                   Error.show();
                   ModifyTask.hide();
            }

            _renderBoard();
          })

          /* A comment has been added to a task. Update comments property of that task.
           * If ModifyTask-dialogue is open, update comments in that modal. */
          .listen('.board.task.comment.added', (e) => {
            var parentTask = JSON.parse(e.task);
            _tasks.get(parentTask.id).comments = parentTask.comments;
            if (ModifyTask.isOpen() &&
                 ModifyTask.currentTask() == parentTask.id) {
                   ModifyTask.setComments(parentTask.comments);
            }
          })

          /* Board title or members with access to the board has been changed.
           * Update _boardTitle and _members properties.
           * Update ModifyBoard and ModifyMembers-dialogues with new data. */
          .listen('.board.updated', (e) => {
            var updatedBoard = JSON.parse(e.board);
            _boardTitle = updatedBoard.name;
            _loadMembers(updatedBoard.members);

            ModifyBoard.update(_boardId, _boardTitle, _boardCreatedAt);
            ModifyMembers.update(_boardId, _boardOwner, _members);
            ModifyTask.update(_boardId, _boardOwner, _members);
            _renderHeader();

            /* If current user no longer has access to this board, display an error message */
            if (!_members.has(_currentUser.id) &&
                _boardOwner.id != _currentUser.id) {
              Error.setMessage('Your access to this board has been revoked. Go back to <a href="/">Dashboard</a>?');
              Error.show();
            }

          })

          /* Board has been deleted. Show error message. */
          .listen('.board.deleted', (e) => {
            Error.setMessage('This board has been deleted. Go back to <a href="/">Dashboard</a>?');
            Error.show();
          })
  }

  /* Initialize dialogue windows with callback methods and initial data. */
  var _setupModals = function() {
    ModifyTask.init(
      function() {
        // Failed to update task
        Error.setMessage('An error occurred while updating the task. Please try again.');
        Error.show();
      },
      function() {
        // Failed to delete task
        Error.setMessage('An error occurred while deleting the task. Please try again.');
        Error.show();
      },
      function() {
        // Failed to add comment
        Error.setMessage('An error occurred while adding your comment. Please try again.');
        Error.show();
      });
    ModifyTask.update(_boardId, _boardOwner, _members);

    CreateTask.init(function() {
      // Failed to create task
      Error.setMessage('An error occurred while creating your task. Please try again.');
      Error.show();
    });
    CreateTask.update(_boardId);

    ModifyBoard.init(
      function() {
        // Failed to modify board
        Error.setMessage('An error occurred while modifying your board. Please try again.');
        Error.show();
      },
      function() {
        // Failed to delete board
        Error.setMessage('An error occurred while deleting your board. Please try again.');
        Error.show();
      });
    ModifyBoard.update(_boardId, _boardTitle, _boardCreatedAt);

    ModifyMembers.init(
      function() {
        // Failed to add member
        Error.setMessage('An error occurred while granting access to your board. Please try again.');
        Error.show();
      },
      function() {
        // Failed to remove member
        Error.setMessage('An error occurred while removing access to your board. Please try again.');
        Error.show();
      });
    ModifyMembers.update(_boardId, _boardOwner, _members);

    /* No need for callbacks in this dialogue, only update with data */
    ArchivedTasks.update(_tasks);
  }

  return {
    init: init
  }

})();
