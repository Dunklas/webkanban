@extends('layouts.app')

@section('page-js')
<script src="/js/board.js"></script>
@endsection

@section('toolbar')
<li><a href="" id="createTaskLink">Create task</a></li>
<li><a href="" id="modifyBoardLink">Properties</a></li>
<li><a href="" id="modifyMembersLink">Members</a></li>
<li><a href="" id="archivedTasksLink">Archived tasks</a></li>
@endsection

@section('content')
<main class="container-fluid">
  <div class="row">
    <div class="col">
      <h1 id="boardHeader" class="text-center"></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <h2 class="text-center">To do</h2>
      <div class="card-container" id="todoContainer">
      </div>
    </div>
    <div class="col-md-4">
      <h2 class="text-center">Doing</h2>
      <div class="card-container" id="doingContainer">
      </div>
    </div>
    <div class="col-md-4">
      <h2 class="text-center">Done</h2>
      <div class="card-container" id="doneContainer">
      </div>
    </div>
  </div>
</main>

<!-- Dialogue windows used in this view -->
@component('dialogues.create_task')
@endcomponent
@component('dialogues.modify_task')
@endcomponent
@component('dialogues.error')
@endcomponent
@component('dialogues.archived_tasks')
@endcomponent

<!-- Board parameter is passed because this dialogue shows different content,
depending on if the user is owner of the board or not. The parameter is
originally passed from a controller object. -->
@component('dialogues.modify_members', ['board' => $board])
@endcomponent

<!-- Board parameter here too... -->
@component('dialogues.modify_board', ['board' => $board])
@endcomponent

@endsection
