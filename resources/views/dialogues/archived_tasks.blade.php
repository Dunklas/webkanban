<!-- Modal (popup window) for archived tasks -->
<div class="modal fade" id="archivedTasks" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Archived tasks</h4>
      </div>
      <div class="modal-body">
        <div class ="archivedTaskContainer">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Title</th>
                <th>Updated at</th>
              </tr>
            </thead>
            <tbody id="archivedTasksTbody">
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
