<!-- Modal (popup window) for modification of a task -->
<div class="modal fade" id="modifyTask" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="taskHeader"></h4>
        </div>
        <div class="modal-body">
          <form id="modifyTaskForm">
            <div class="form-group">
              <label for="taskTitle">Title</label>
              <input type="text" class="form-control" id="taskTitle">
            </div>
            <div class="form-group">
              <label for="taskStatus">Status</label>
              <select class="form-control" id="taskStatus">
                <option value="todo">To do</option>
                <option value="doing">Doing</option>
                <option value="done">Done</option>
                <option value="archived">Archived</option>
              </select>
            </div>
            <div class="form-group">
              <label for="taskAssignee">Assigned to</label>
              <select class="form-control" id="taskAssignee">
              </select>
            </div>
            <div class="form-group">
              <label for="taskComment">Add comment</label>
              <div class="input-group">
                <input type="text" id="taskComment" class="form-control">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="button" id="taskAddComment">Add</button>
                </span>
              </div>
            </div>
            <div class="taskLogContainer">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>User</th>
                    <th>Date/Time</th>
                    <th>Comment</th>
                  </tr>
                </thead>
                <tbody id="taskLog">
                </tbody>
              </table>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <span class="pull-left">
            <button type="button" id ="taskDelete" class="btn btn-default">Delete task</button>
          </span>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button id="taskModify" type="button" class="btn btn-primary">Save changes</button>
        </div>
    </div>
  </div>
</div>
