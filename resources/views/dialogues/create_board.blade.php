<!-- Modal (popup window) for creation of a board -->
<div class="modal fade" id="createBoard" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create board</h4>
        </div>
        <div class="modal-body">
          <form id="createBoardForm">
            <div class="form-group">
              <input type="text" id="newBoardTitle" class="form-control" placeholder="Enter a board title">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" id="createBoardBtn" class="btn btn-primary">Create</button>
        </div>
      </form>
    </div>
  </div>
</div>
