<!-- Modal (popup window) for creation of tasks -->
<div class="modal fade" id="createTask" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create task</h4>
        </div>
        <div class="modal-body">
          <form id="createTaskForm">
            <div class="form-group">
              <input type="text" id="newTaskTitle" class="form-control" placeholder="Enter a task title">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" id="taskSave" class="btn btn-primary">Create</button>
        </div>
      </form>
    </div>
  </div>
</div>
