<!-- Modal (popup window) for modification of a board -->
<div class="modal fade" id="modifyBoard" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Properties</h4>
        </div>
        <div class="modal-body">
          <form id="modifyBoardForm">
            <div class="form-group">
              <label for="boardTitle">Board title</label>
              @if(Auth::user()->isOwner($board))
              <input type="text" class="form-control" id="boardTitle" placeholder="Enter a board title">
              @else
              <input type="text" class="form-control" id="boardTitle" readonly>
              @endif
            </div>
          </form>
          <p>Created: <span id="boardCreated"></span></p>
        </div>
        <div class="modal-footer">
          @if(Auth::user()->isOwner($board))
          <span class="pull-left">
            <button type="button" id ="boardDelete" class="btn btn-default">Delete board</button>
          </span>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" id="boardSave" class="btn btn-primary">Save changes</button>
          @else
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          @endif
        </div>
      </form>
    </div>
  </div>
</div>
