<!-- Modal (popup window) for adding/removing members to a board -->
<div class="modal fade" id="modifyMembers" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Members</h4>
        </div>
        <div class="modal-body">
          @if(Auth::user()->isOwner($board))
          <form id="modifyMembersForm">
            <div class="form-group">
              <label for="memberEmail">Add member</label>
              <div class="input-group">
                <input type="email" id="memberEmail" class="form-control" placeholder="Enter e-mail address of a member">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="button" id="memberAdd">Add</button>
                </span>
              </div>
            </div>
          </form>
          @endif
          <ul class="list-group" id="memberList">
            <li class="list-group-item" id="memberOwner"></li>
            @if(Auth::user()->isOwner($board))
            <div class="memberTemplate">
              <li class="list-group-item">
                <span class="pull-right">
                  <button type="button" class="btn btn-xs btn-default memberRemove">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                  </button>
                </span>
              </li>
            </div>
            @else
            <div class="memberTemplate">
              <li class="list-group-item"></li>
            </div>
            @endif
          </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>
