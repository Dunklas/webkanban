@extends('layouts.app')

@section('content')
<main class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
        <form action="register" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <fieldset>
            <legend>Soon there...</legend>

            @if(Session::has('success'))
            <div class="alert alert-success">
              <p>{{Session::get('success')}}.</p>
              <p>Click <a href="{{URL::to('/login')}}">here</a> to login.</p>
            </div>
            @endif

            <p>In order to use WebKanban you need to create an account. The accounts are needed to keep track who should see what kanban board :)</p>

            @if($errors->has('email'))
            <div class="form-group has-error">
            @else
            <div class="form-group">
            @endif
              <label for="email">Email:</label>
              <input type="email" id="email" name ="email" placeholder="Enter email" class="form-control">
              @if($errors->has('email'))
                <span class="help-block">{{$errors->first('email')}}</span>
              @endif
            </div>

            @if($errors->has('password'))
            <div class="form-group has-error">
            @else
            <div class="form-group">
            @endif
              <label for="password">Password:</label>
              <input type="password" id="password" name="password" placeholder="Enter password" class="form-control">
              @if($errors->has('password'))
                <span class="help-block">{{$errors->first('password')}}</span>
              @endif
            </div>

            @if($errors->has('confirm_password'))
            <div class="form-group has-error">
            @else
            <div class="form-group">
            @endif
              <label for"confirm_password">Confirm password:</label>
              <input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm password" class="form-control">
              @if($errors->has('confirm_password'))
                <span class="help-block">{{$errors->first('confirm_password')}}</span>
              @endif
            </div>
            <div class="form-group">
              <input type="submit" value ="Register" class="btn btn-primary">
            </div>
          </fieldset>
        </form>
      </div>
    </div>
</main>
@endsection
