<!-- This template is parent to all views. It includes a navigation bar and a
     few "slots" where child views can insert content, js-files, etc -->

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="/css/app.css" rel="stylesheet">
  <script src="/js/app.js"></script>
  <script src="/js/all.js"></script>

  <!-- Here, a child view may insert <script>-tags -->
  @yield('page-js')

  <title>WebKanban</title>
</head>
<body>
  <nav class="navbar navbar-inverse navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{URL::to('/')}}">WebKanban</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-left">
        <!-- Here, a child view may insert options to be displayed in nav bar -->
        @yield('toolbar')
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <!-- Options in navbar to be displayed for users who are not logged in -->
        @guest
        <li><a href="{{URL::to('/register')}}"><span class="glyphicon glyphicon-user"></span> Register</a></li>
        <li><a href="{{URL::to('/login')}}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        @endguest

        <!-- Options in navbar to be displayed for users who are logged in -->
        @auth
        <p class="navbar-text hidden-sm hidden-xs">{{Auth::user()->email}}</p>
        <li><a href="{{URL::to('/logout')}}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
        @endauth
      </ul>
    </div>
  </div>
</nav>

<!-- Here, content is inserted by child views -->
@yield('content')

</body>
</html>
