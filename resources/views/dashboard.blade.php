@extends('layouts.app')

@section('page-js')
<script src="/js/dashboard.js"></script>
@endsection

@section('content')
<main class="container-fluid">
</div>
<div class="row">
  <div class="col-md-6">
    <h1 class="text-center">My Boards</h2>
    <div class="card-container" id="myBoardsContainer">
      <div class="card card-primary" id="createBoardCard">
        <span class="card-title">Create new</span>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <h1 class="text-center">Shared with me</h2>
    <div class="card-container" id="sharedBoardsContainer">
    </div>
  </div>
</div>
</main>

<!-- Dialogue windows used in this view -->
@component('dialogues.create_board')
@endcomponent
@component('dialogues.error')
@endcomponent

@endsection
