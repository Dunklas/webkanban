@extends('layouts.app')

@section('content')
<main class="container">
  <div class="row">
    <div class="col">
      <div class="jumbotron jumbotron-transparent">
        <h1 class="text-center">A simple kanban board</h1>
        <p class="text-center">Login below or click <a href="{{URL::to('/register')}}">here</a> to register</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
        <form action="login" method="post">
          <fieldset>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @if($errors->has('email'))
            <div class="form-group has-error">
            @else
            <div class="form-group">
            @endif
              <label for="email">Email:</label>
              <input type="email" id="email" name="email" placeholder="Enter email" class="form-control">
              @if($errors->has('email'))
                <span class="help-block">{{$errors->first('email')}}</span>
              @endif
            </div>

            @if($errors->has('password'))
            <div class="form-group has-error">
            @else
            <div class="form-group">
            @endif
              <label for="password">Password:</label>
              <input type="password" id="password" name="password" placeholder="Enter password" class="form-control">
              @if($errors->has('password'))
                <span class="help-block">{{$errors->first('password')}}</span>
              @endif
              @if(Session::has('authFail'))
                <span class="help-block">{{Session::get('authFail')}}</span>
              @endif
            </div>
            <div class="form-group">
              <input type="submit" value="Login" class="btn btn-primary">
            </div>
          </fieldset>
        </form>
    </div>
  </div>
</main>
@endsection
