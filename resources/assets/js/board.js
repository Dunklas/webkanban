/* This js-file is used in view(s) where a board is displayed. */

$("#document").ready(function() {

  /* Extract id from url path */
  var pathArr = location.pathname.split('/');
  var board_id = pathArr[pathArr.length-1];

  Board.init(board_id);

});

/* Uses module pattern to encapsulate logic related to board-view. */
var Board = (function () {

  var _currentUser = null;

  var _boardId = null;
  var _boardTitle = null;
  var _boardOwner = null;
  var _boardCreatedAt = null;
  var _members = null;
  var _tasks = null;

  /* After a board has been fetched (GET), methods are called to setup this page/view */
  init = function(board_id) {
    _loadBoard(board_id, function() {
      // On successfully loaded board
      _renderBoard();
      _setupHandlers();
      _setupModals();
      _setupEventListeners();

      /* Store current user in order to enable comparison to check if events originates from this user. */
      Auth.currentUser(function(user) {
        _currentUser = user;
      });
    });
  }

  /* Load a board with a GET request. Store data from JSON response in variables with a leading underscore */
  var _loadBoard = function(board_id, onSuccess) {
    $.get("/board/" + board_id, function(response) {
      rJson = JSON.parse(response);

      _boardId = rJson.id;
      _boardTitle = rJson.name;
      _boardOwner = rJson.owner;
      _boardCreatedAt = rJson.created_at;

      _loadMembers(rJson.members);
      _loadTasks(rJson.tasks);

      onSuccess();
    }).fail(function() {
      Error.setMessage('An error occurred while loading the board. Please try again later.');
      Error.show();
    });
  }

  /* Store members in a map object */
  var _loadMembers = function(members) {
    _members = new Map();
    $.each(members, function(i, member) {
      _members.set(member.id, member);
    });
  }

  /* Store tasks in a map object */
  var _loadTasks = function(tasks) {
    _tasks = new Map();
    $.each(tasks, function(i, task) {
      _tasks.set(task.id, task);
    });
  }

  /* Called to render a board on the page */
  var _renderBoard = function() {
    _renderHeader();
    $('.card-container').html('');
    for (var [id, task] of _tasks) {
      if (task.status != 'archived') {
        _renderTask(task);
      }
    }
  }

  /* Show header */
  var _renderHeader = function() {
    $('#boardHeader').html(_boardTitle);
  }

  /* Show each task in a div element. Each tasks' id is inserted to a hidden input element.
   * The id is needed to build url paths to be used if user clicks on a task */
  var _renderTask = function(task) {
    var taskHTML = "";
    taskHTML += '<div class="card card-postit task">';
    taskHTML += '<input type="hidden" value="' + task.id + '">';
    taskHTML += '<span class="card-title">' + task.name + '</span>';
    taskHTML += '</div>';

    /* Different containers depending on status of task */
    switch (task.status) {
      case 'todo':
        $('#todoContainer').append(taskHTML);
        break;
      case 'doing':
        $('#doingContainer').append(taskHTML);
        break;
      case 'done':
        $('#doneContainer').append(taskHTML);
        break;
    }
  }

  /* Removes a div element representing a task */
  var _unrenderTask = function(task) {
    $('.card-container :input[value="' + task.id + '"]').parent().remove();
  }

  /* Setup handlers for click events */
  var _setupHandlers = function() {
    /* When create task-link is clicked, show modal. */
    $('#createTaskLink').on('click', function(evt) {
      evt.preventDefault();
      CreateTask.show();
    });

    /* When members-link is clicked, show modal. */
    $('#modifyMembersLink').on('click', function(evt) {
      evt.preventDefault();
      ModifyMembers.show();
    });

    /* When properties-link is clicked, show modal. */
    $('#modifyBoardLink').on('click', function(evt) {
      evt.preventDefault();
      ModifyBoard.show();
    });

    /* When archived tasks-link is clicked, show modal. */
    $('#archivedTasksLink').on('click', function(evt) {
      evt.preventDefault();
      ArchivedTasks.show();
    });

    /* When a task is clicked, show modal. Uses event delegation since the task
     * elements are created after document is loaded. */
    $('.card-container').on('click', '.task', function() {
      var taskId = Number($(this).find('>:first-child').val());
      ModifyTask.setTask(_tasks.get(taskId));
      ModifyTask.setComments(_tasks.get(taskId).comments);
      ModifyTask.show();
    });
  }

  /* Setup listeners using Laravel Echo. Listens to events broadcasted from backend
   * via Pusher. */
  var _setupEventListeners = function() {
    Echo.private('board.' + _boardId)

          /* New task created, add to _tasks and re-render the board */
          .listen('.board.task.created', (e) => {
            var newTask = JSON.parse(e.task);
            _tasks.set(newTask.id, newTask);
            _renderBoard();
          })

          /* Task has been updated. Update object in _tasks and re-render the board
           * If Modify task-dialogue is open, update data in that modal
           * Update ArchivedTasks-dialogue in case a task has been archived */
          .listen('.board.task.updated', (e) => {
            var updatedTask = JSON.parse(e.task);
            _tasks.set(updatedTask.id, updatedTask);

            if (ModifyTask.isOpen() &&
                 ModifyTask.currentTask() == updatedTask.id) {
                   ModifyTask.setTask(updatedTask);
            }

            ArchivedTasks.update(_tasks);

            _renderBoard();
          })

          /* Task has been deleted. Delete object from _tasks and re-render board.
           * If the user who removed the board is not the current user, show an error message */
          .listen('.board.task.deleted', (e) => {
            var deletedTask = JSON.parse(e.task);
            _tasks.delete(deletedTask.id);

            var changer = JSON.parse(e.user);

            if (ModifyTask.isOpen() &&
                 ModifyTask.currentTask() == deletedTask.id &&
                   changer.id != _currentUser.id) {
                   Error.setMessage('The task you are currently viewing or modifying has been deleted by ' + changer.email + '.');
                   Error.show();
                   ModifyTask.hide();
            }

            _renderBoard();
          })

          /* A comment has been added to a task. Update comments property of that task.
           * If ModifyTask-dialogue is open, update comments in that modal. */
          .listen('.board.task.comment.added', (e) => {
            var parentTask = JSON.parse(e.task);
            _tasks.get(parentTask.id).comments = parentTask.comments;
            if (ModifyTask.isOpen() &&
                 ModifyTask.currentTask() == parentTask.id) {
                   ModifyTask.setComments(parentTask.comments);
            }
          })

          /* Board title or members with access to the board has been changed.
           * Update _boardTitle and _members properties.
           * Update ModifyBoard and ModifyMembers-dialogues with new data. */
          .listen('.board.updated', (e) => {
            var updatedBoard = JSON.parse(e.board);
            _boardTitle = updatedBoard.name;
            _loadMembers(updatedBoard.members);

            ModifyBoard.update(_boardId, _boardTitle, _boardCreatedAt);
            ModifyMembers.update(_boardId, _boardOwner, _members);
            ModifyTask.update(_boardId, _boardOwner, _members);
            _renderHeader();

            /* If current user no longer has access to this board, display an error message */
            if (!_members.has(_currentUser.id) &&
                _boardOwner.id != _currentUser.id) {
              Error.setMessage('Your access to this board has been revoked. Go back to <a href="/">Dashboard</a>?');
              Error.show();
            }

          })

          /* Board has been deleted. Show error message. */
          .listen('.board.deleted', (e) => {
            Error.setMessage('This board has been deleted. Go back to <a href="/">Dashboard</a>?');
            Error.show();
          })
  }

  /* Initialize dialogue windows with callback methods and initial data. */
  var _setupModals = function() {
    ModifyTask.init(
      function() {
        // Failed to update task
        Error.setMessage('An error occurred while updating the task. Please try again.');
        Error.show();
      },
      function() {
        // Failed to delete task
        Error.setMessage('An error occurred while deleting the task. Please try again.');
        Error.show();
      },
      function() {
        // Failed to add comment
        Error.setMessage('An error occurred while adding your comment. Please try again.');
        Error.show();
      });
    ModifyTask.update(_boardId, _boardOwner, _members);

    CreateTask.init(function() {
      // Failed to create task
      Error.setMessage('An error occurred while creating your task. Please try again.');
      Error.show();
    });
    CreateTask.update(_boardId);

    ModifyBoard.init(
      function() {
        // Failed to modify board
        Error.setMessage('An error occurred while modifying your board. Please try again.');
        Error.show();
      },
      function() {
        // Failed to delete board
        Error.setMessage('An error occurred while deleting your board. Please try again.');
        Error.show();
      });
    ModifyBoard.update(_boardId, _boardTitle, _boardCreatedAt);

    ModifyMembers.init(
      function() {
        // Failed to add member
        Error.setMessage('An error occurred while granting access to your board. Please try again.');
        Error.show();
      },
      function() {
        // Failed to remove member
        Error.setMessage('An error occurred while removing access to your board. Please try again.');
        Error.show();
      });
    ModifyMembers.update(_boardId, _boardOwner, _members);

    /* No need for callbacks in this dialogue, only update with data */
    ArchivedTasks.update(_tasks);
  }

  return {
    init: init
  }

})();
