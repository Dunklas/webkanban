var Auth = (function () {

  /* This function returns a JSON representation of the authenticated user */
   currentUser = function(onSuccess) {
     $.ajax({
       url: '/whoami/',
       type: 'GET',
       success: function(response) {
         onSuccess(response);
       },
       error: function(response) {
         return false;
       }
     });
   }

   return {
     currentUser: currentUser
   }

}());
