/* Uses module pattern to encapsulate logic related to ModifyTask-dialogue */
var ModifyTask = (function () {

  var _isOpen = false;

  var _boardId = null;
  var _boardOwner = null;
  var _members = null;

  var _taskId = null;
  var _taskTitle = null;
  var _taskStatus = null;
  var _taskAssignee = null;
  var _taskComments = null;

  var _onTaskUpdateFailure = null;
  var _onTaskDeleteFailure = null;
  var _onCommentAddFailure = null;

  /* Setup callback functions and invoke _setup() */
  init = function (onTaskUpdateFailure, onTaskDeleteFailure, onCommentAddFailure) {
    _onTaskUpdateFailure = onTaskUpdateFailure;
    _onTaskDeleteFailure = onTaskDeleteFailure;
    _onCommentAddFailure = onCommentAddFailure;
    _setup();
  };

  /* Show dialogue */
  show = function() {
    _isOpen = true;
    $('#modifyTask').modal('show');
  }

  /* Hide dialogue */
  hide = function() {
    $('#modifyTask').modal('hide');
  }

  /* Returns a boolean indicating if this dialogue is open or not */
  isOpen = function() {
    return _isOpen;
  }


  /* Returns the id of the task currently displayed in this dialogue. */
  currentTask = function() {
    return _taskId;
  }


  /* Updates this modal with new state.
   * This is needed when any property of a board, or members
   * of a board has changed. These properties are the same,
   * regardless of which task is open in this dialogue. */
  update = function(boardId, boardOwner, members) {
    _boardId = boardId;
    _boardOwner = boardOwner;
    _members = members;

    _renderCommon();
  }


  /* Updates this modal with state of a specific task.
   * This is normally done right before this dialogue is opened.
   * However, it may be updated while open if another user has
   * made an update to the task currently open in this dialogue. */
  setTask = function(task) {
    _taskId = task.id;
    _taskTitle = task.name;
    _taskStatus = task.status;
    _taskAssignee  = task.assigned_to;
    _renderTask();
  }


  /* Updates this modal with comments of a specific task.
   * This could be included in setTask(task), but I separated
   * them in order to be able to update task data and comments separately.
   * Otherwise, the following scenario could happen:
   * 1. User writes a comment but do NOT click add-button.
   * 2. User changes status of task.
   * 3. User click add-button.
   * 4. Status is reverted back to previous value when comment is added. */
  setComments = function(comments) {
    _taskComments = comments;
    _renderComments();
  }


  /* Setup event handlers for click events, etc used in this dialogue. */
  var _setup = function() {
    /* Hide validation errors when modal is hidden. */
    $('#modifyTask').on('hidden.bs.modal', function() {
      _isOpen = false;
      _hideValidationErrors();
    });

    /* Invoke _updateTask when user click save button. */
    $('#taskModify').on('click', function() {
      _hideValidationErrors();
      var title = $('#taskTitle').val();
      var status = $('#taskStatus').val();
      var assignee = $('#taskAssignee').val();
      _updateTask(title, status, assignee);
    });

    /* Invoke _addComment when user click add comment button. */
    $('#taskAddComment').on('click', function() {
      _hideValidationErrors();
      var message = $('#taskComment').val();
      _addComment(message);
    });

    /* Invoke _deleteTask when user click delete button. */
    $('#taskDelete').on('click', function() {
      _deleteTask();
    });
  }


  /* Render properties which are common for this dialogue,
   * regardless of which task is opened. */
  var _renderCommon = function() {
    $('#taskAssignee').html('');

    var memberHTML;
    memberHTML += _getUserOption(_boardOwner);
    for (var [id, member] of _members) {
      memberHTML += _getUserOption(member);
    }
    $('#taskAssignee').append(memberHTML);

    if (_taskAssignee != null) {
      $('#taskAssignee').val(_taskAssignee);
    }
  }

  /* Returns an <option>-element for provided member. */
  var _getUserOption = function(member) {
    var html = '<option value="' + member.id + '"';
    html += '>' + member.email + '</option>'
    return html;
  }


  /* Render properties which are specific to the
   * task which should be displayed in this dialogue. */
  var _renderTask = function() {
    /* Set title */
    $('#taskHeader').html(_taskTitle);
    $('#taskTitle').val(_taskTitle);

    /* Set status */
    $('#taskStatus').val(_taskStatus);

    /* Set selected member to the task assignee */
    $('#taskAssignee').val(_taskAssignee);
  }


  /* Render comments which are specific to
   * the task displayed in this dialogue. */
  var _renderComments = function() {
    $('#taskLog').html('');
    $.each(_taskComments, function(i, comment) {
      _renderComment(comment);
    });
  }

  /* Renders HTML elements for one comment */
  var _renderComment = function(comment) {
    var commentHTML = '<tr><td>';
    commentHTML += comment.author.email + '</td><td>';
    commentHTML += toLocalDateTimeString(comment.created_at) + '</td><td>';
    commentHTML += comment.message + '</td></tr>'
    $('#taskLog').prepend(commentHTML);
  }


  /* Sends a HTTP PUT request in order to update this task. */
  var _updateTask = function(title, status, assignee) {

    $.ajax({
      url: '/board/' + _boardId + '/task/' + _taskId,
      type: 'PUT',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        'title': title,
        'status' : status,
        'assigned_to' : assignee
      },
      success: function(response) {
        $('#modifyTask').modal('hide');
      },
      error: function(response) {
        /* Validation errors, show them */
        if (response.responseJSON.errors) {
          _showValidationErrors(response.responseJSON);
        } else {
          /* Other error, invoke callback function */
          $('#modifyTask').modal('hide');
          _onTaskUpdateFailure();
        }
      }
    });
  }


  /* Sends a HTTP DELETE request in order to delete this task. */
  var _deleteTask = function() {
    $.ajax({
      url: '/board/' + _boardId + '/task/' + _taskId,
      type: 'DELETE',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      success: function(response) {
        $('#modifyTask').modal('hide');
      },
      error: function() {
        /* Invoke callback function */
        $('#modifyTask').modal('hide');
        _onTaskDeleteFailure();
      }
    });
  }


  /* Sends a HTTP POST request to add a comment to this task. */
  var _addComment =  function(message) {
    $.ajax({
      url: '/board/' + _boardId + '/task/' + _taskId + '/comment',
      type: 'POST',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        'comment': message
      },
      success: function(response) {
        $('#taskComment').val('');
      },
      error: function(response) {
        /* Validation errors, show them! */
        if (response.responseJSON.errors) {
          _showValidationErrors(response.responseJSON);
        } else {
          /* Invoke callback function */
          $('#taskComment').val('');
          _onCommentAddFailure();
        }
      }
    });
  }


  /* Shows validation errors in this dialogue.
   * Server sends a JSON response with validation errors,
   * which is expected as parameter to this function. */
  var _showValidationErrors = function(response) {
    if (response.errors.title) {
      $('#taskTitle').parent().addClass('has-error');
      $('#taskTitle').parent().append(
        '<span class="help-block">' + response.errors.title + '</span>'
      );
    } else if (response.errors.comment) {
      $('#taskComment').parent().parent().addClass('has-error');
      $('#taskComment').parent().parent().append(
        '<span class="help-block">' + response.errors.comment + '</span>'
      )
    }
  }


  /* Removes all validation errors. */
  var _hideValidationErrors = function(response) {
    $('#modifyTaskForm .has-error').removeClass('has-error');
    $('#modifyTaskForm .help-block').remove();
  }

  /* Returns publically exposed methods. */
  return {
    init: init,
    update: update,
    show: show,
    hide: hide,
    isOpen : isOpen,
    currentTask : currentTask,
    setTask : setTask,
    setComments : setComments
  };

})();
