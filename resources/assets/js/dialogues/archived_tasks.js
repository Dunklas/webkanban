/* Uses module pattern to encapsulate logic related to ArchivedTasks-dialogue */
var ArchivedTasks = (function () {

  /* Re-renders this dialogue with new data */
  update = function(tasks) {
    $('#archivedTasksTbody').html('');
    for (var [id, task] of tasks) {
      /* Only render tasks with status 'archived' */
      if (task.status == 'archived') {
        _renderTask(task);
      }
    }
  }

  /* Show dialogue */
  show = function() {
    $('#archivedTasks').modal('show');
  }

  /* Render HTML-elements for one task */
  var _renderTask = function(task) {
    var taskHTML = '<tr><td>';
    taskHTML += task.name + '</td><td>';
    taskHTML += toLocalDateTimeString(task.updated_at) + '</td></tr>';
    $('#archivedTasksTbody').prepend(taskHTML);
  }

  return {
    update: update,
    show: show
  }

})();
