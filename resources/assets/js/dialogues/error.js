/* Uses module pattern to encapsulate logic related to Error-dialogue */
var Error = (function () {

  var _message = null;

  /* Set message for this dialogue */
  setMessage = function(message) {
    _message = message;
    $('#errorMsg').html(_message);
  }

  /* Show dialogue */
  show = function() {
    $('#error').modal('show');
  }

  return {
    setMessage: setMessage,
    show: show
  }

})();
