/* Uses module pattern to encapsulate logic related to ModifyBoard-dialogue */
var ModifyBoard = (function () {

  var _boardId = null;
  var _boardTitle = null;
  var _boardCreatedAt = null;

  var _onBoardModifyFailure = null;
  var _onBoardDeleteFailure = null;

  /* Setup callback functions and invoke _setup() */
  init = function(onBoardModifyFailure, onBoardDeleteFailure) {
    _onBoardModifyFailure = onBoardModifyFailure;
    _onBoardDeleteFailure = onBoardDeleteFailure;
    _setup();
  };

  /* Update dialogue with new data about this board, and re-render dialogue */
  update = function(boardId, boardTitle, boardCreatedAt) {
    _boardId = boardId;
    _boardTitle = boardTitle;
    _boardCreatedAt = boardCreatedAt;
    _renderDialogue();
  };

  /* Show dialogue */
  show = function() {
    $('#modifyBoard').modal('show');
  };

  /* Setup handlers for click events */
  var _setup = function() {
    /* Hide validation errors when modal is hidden */
    $('#modifyBoard').on('hidden.bs.modal', function() {
      _hideValidationErrors();
    });

    $('#boardDelete').on('click', function() {
      _deleteBoard();
    });

    /* Hide validation errors when save-button is clicked */
    $('#boardSave').on('click', function() {
      _hideValidationErrors();

      var newTitle = $('#boardTitle').val();
      _modifyBoard(newTitle);
    });
  }

  /* Render data about this board */
  var _renderDialogue = function() {
    $('#boardTitle').val(_boardTitle);
    $('#boardCreated').html(toLocalDateTimeString(_boardCreatedAt));
  }

  /* Sends a PATCH request to update this board */
  var _modifyBoard = function(title) {
    $.ajax({
      url: '/board/' + _boardId,
      type: 'PATCH',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        'attribute': 'title',
        'operation' : 'update',
        'value' : title
      },
      success: function(response) {
        /* Success, hide this modal */
        $('#modifyBoard').modal('hide');
      },
      error: function(response) {
        /* Validation errors, show them */
        if (response.responseJSON.errors) {
          _showValidationErrors(response.responseJSON);
        } else {
          /* Other error, hide modal and invoke callback function */
          $('#modifyBoard').modal('hide');
          _onBoardModifyFailure();
        }
      }
    });
  }

  /* Sends a DELETE request to delete this board */
  var _deleteBoard = function() {
    $.ajax({
      url: '/board/' + _boardId,
      type: 'DELETE',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      success: function(response) {
        /* Successful, hide modal */
        $('#modifyBoard').modal('hide');
      },
      error: function() {
        /* Failure, hide modal and invoke callback function */
        $('#modifyBoard').modal('hide');
        _onBoardDeleteFailure();
      }
    });
  }

  var _showValidationErrors = function(response) {
    if (response.errors.value) {
      $('#boardTitle').parent().addClass('has-error');
      $('#boardTitle').parent().append(
        '<span class="help-block">' + response.errors.value + '</span>'
      );
    }
  }

  var _hideValidationErrors = function() {
    $('#modifyBoardForm .has-error').removeClass('has-error');
    $('#modifyBoardForm .help-block').remove();
  }

  return {
    init: init,
    update: update,
    show: show
  }

})();
