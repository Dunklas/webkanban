/* Uses module pattern to encapsulate logic related to CreateTask-dialogue */
var CreateTask = (function () {

  var _boardId = null;
  var _onTaskCreateFailure = null;

  /* Update this modal with a board id (needed for path building for HTTP requests) */
  update = function(boardId) {
    _boardId = boardId;
  }

  /* Show dialogue */
  show = function() {
    $('#createTask').modal('show');
  }

  /* Setup callback function and invoke _setup() */
  init = function(onTaskCreateFailure) {
    _onTaskCreateFailure = onTaskCreateFailure;
    _setup();
  }

  /* Setup handlers for click events */
  var _setup = function() {
    /* Set focus on first field when modal is displayed. */
    $('#createTask').on('shown.bs.modal', function() {
      $('#newTaskTitle').focus();
    });
    /* Clear HTML field when modal is hidden. */
    $('#createTask').on('hidden.bs.modal', function() {
      $('#newTaskTitle').val("");
      _hideValidationErrors();
    });
    /* Invoke _createTask when user click save button. */
    $('#taskSave').on('click', function() {
      _hideValidationErrors();

      var task_name = $('#newTaskTitle').val();
      _createTask(task_name);
    });
  }

  /* Send POST request to create a task */
  var _createTask = function(new_task_title) {
    $.ajax({
      url: '/board/' + _boardId + '/task',
      type: 'POST',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {'title': new_task_title},
      success: function(response) {
        /* Successful, hide modal */
        $('#createTask').modal('hide');
      },
      error: function(response) {
        /* Validation errors, show them! */
        if (response.responseJSON.errors) {
          _showValidationErrors(response.responseJSON);
        } else {
          /* Other error, hide dialogue and invoke callback function */
          $('#createTask').modal('hide');
          _onTaskCreateFailure();
        }
      }
    });
  }

  var _showValidationErrors = function(response) {
    if (response.errors.title) {
      $('#newTaskTitle').parent().addClass('has-error');
      $('#newTaskTitle').parent().append(
        '<span class="help-block">' + response.errors.title + '</span>'
      );
    }
  }

  var _hideValidationErrors = function() {
    $('#createTaskForm .has-error').removeClass('has-error');
    $('#createTaskForm .help-block').remove();
  }

  return {
    init: init,
    show: show,
    update: update
  }

})();
