/* Uses module pattern to encapsulate logic related to CreateBoard-dialogue */
var CreateBoard = (function () {

  /* Callback functions for success/failure when creating a board */
  var _onSuccess = null;
  var _onFailure = null;

  /* Setup callback methods and call _setup() to initialize handlers for click events */
  init = function(onSuccess, onFailure) {
    _onSuccess = onSuccess;
    _onFailure = onFailure;
    _setup();
  }

  /* Show this dialogue */
  show = function() {
    $('#createBoard').modal('show');
  }

  /* Setup handlers for click events */
  var _setup = function() {
    /* Focus on first text field on open */
    $('#createBoard').on('shown.bs.modal', function() {
      $('#newBoardTitle').focus();
    });

    /* When modal is hidden, clear HTML-field(s) and hide validation errors */
    $('#createBoard').on('hidden.bs.modal', function() {
      $('#newBoardTitle').val("");
      _hideValidationErrors();
    });

    /* When create button is clicked, hide validation errors, clear HTML-field(s) and invoke _clearBoard(title) */
    $('#createBoardBtn').on('click', function() {
      _hideValidationErrors();

      var title = $('#newBoardTitle').val();
      _createBoard(title);
    });
  }

  /* Sends POST request to create a board */
  var _createBoard = function(title) {
      $.ajax({
        url: '/board',
        type: 'POST',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data: {'title': title},
        success: function(response) {
          /* Successful. Hide modal and invoke callback function. */
          $('#createBoard').modal('hide');
          if (_onSuccess != null) {
            _onSuccess();
          }
        },
        error: function(response) {
          /* If validation errors, show them */
          if (response.responseJSON.errors) {
            _showValidationErrors(response.responseJSON);
          } else {
            /* Other failure, hide modal and invoke callback function. */
            if (_onFailure != null) {
              _onFailure();
            }
            $('#createBoard').modal('hide');
          }
        }
      });
  }

  var _showValidationErrors = function(response) {
    if (response.errors.title) {
      $('#newBoardTitle').parent().addClass('has-error');
      $('#newBoardTitle').parent().append(
        '<span class="help-block">' + response.errors.title + '</span>'
      );
    }
  }

  var _hideValidationErrors = function() {
    $('#createBoardForm .has-error').removeClass('has-error');
    $('#createBoardForm .help-block').remove();
  }

  return {
    init: init,
    show: show
  }

})();
