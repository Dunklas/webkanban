/* Uses module pattern to encapsulate logic related to ModifyMembers-dialogue */
var ModifyMembers = (function () {

  var _boardId;
  var _boardOwner;
  var _members;

  var _onAddMemberFailure = null;
  var _onRemoveMemberFailure = null;

  /* HTML elements to be used to render owner and each member of this board.
   * The elements are generated differently by server depending on if
   * current user is owner of the board or not. Therefore we need a template. */
  var _listTemplate = null;

  /* Update with new data and re-render dialogue */
  update = function(boardId, boardOwner, members) {
    _boardId = boardId;
    _boardOwner = boardOwner;
    _members = members;
    _renderDialogue();
  }

  /* Show dialogue */
  show = function() {
    $('#modifyMembers').modal('show');
  }

  /* Setup callback functions and invoke _setup() */
  init = function(onAddMemberFailure, onRemoveMemberFailure) {
    _onAddMemberFailure = onAddMemberFailure;
    _onRemoveMemberFailure = onRemoveMemberFailure;
    _setup();
  }

  /* Setup handlers for click events, etc */
  var _setup = function() {
    /* This template is used for each member of this board.
     * The template looks different, depending on if the user is owner of the board or not. */
    _listTemplate = $('.memberTemplate').first().clone();
    $('.memberTemplate').remove();

    $('#memberAdd').on('click', function() {
      _hideValidationErrors();

      var email = $('#memberEmail').val();
      _addMember(email);
    });

    /* Uses event delegation, since these li elements are not present initially
     * The button invoking this function is used to remove a member in this board. */
    $('#memberList').on('click', '.memberRemove', function() {
      /* Extract user id */
      var user_email = $(this).closest('li').contents().get(0).nodeValue;
      _removeMember(user_email);
    });

    $('#modifyMembers').on('hidden.bs.modal', function() {
      $('#memberEmail').val('');
      _hideValidationErrors();
    });
  }

  /* Render HTML elements based on data provided in update()-function */
  var _renderDialogue = function() {
    /* Clear member list */
    $('#memberOwner').html('');
    $('#memberList li:not(:first-child)').remove();

    /* Set owner list item */
    $('#memberOwner').html('<strong>' + _boardOwner.email + '</strong>');

    /* Render each member */
    for (var [id, member] of _members) {
      _renderMember(member);
    }

  }

  /* Invoked to render HTML elements for one members */
  var _renderMember = function(member) {
    /* Clone template and prepend email of member */
    var memberElement = _listTemplate.clone().find('>:first-child')
      .prepend(member.email);
    $('#memberList').append(memberElement);
  }

  var _unrenderMember = function(member) {
    $('#memberList li:contains("' + member.email + '")').remove();
  }

  /* Sends a PATCH request to add a member to this board */
  var _addMember = function(user_email) {
    $.ajax({
      url: '/board/' + _boardId,
      type: 'PATCH',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        'attribute': 'members',
        'operation' : 'add',
        'value' : user_email
      },
      success: function(response) {
        /* Do nothing, since events broadcasting (websockets/pusher) to clients take
           care of any rendering */
      },
      error: function(response) {
        /* Validation errors, show them! */
        if (response.responseJSON.errors) {
          _showValidationErrors(response.responseJSON);
        } else {
          /* Invoke callback function */
          _onAddMemberFailure();
        }
      }
    });
  }

  /* Sends a PATCH request to remove a member from this board */
  var _removeMember = function(user_email) {
    $.ajax({
      url: '/board/' + _boardId,
      type: 'PATCH',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data: {
        'attribute' : 'members',
        'operation' : 'remove',
        'value' : user_email
      },
      success: function(response) {
        /* Do nothing, since events broadcasting (websockets/pusher) to clients take
           care of any rendering */
      },
      error: function(response) {
        /* Invoke callback function */
        _onRemoveMemberFailure();
      }
    });
  }

  var _showValidationErrors = function(response) {
    if (response.errors.value) {
      $('#memberEmail').parent().parent().addClass('has-error');
      $('#memberEmail').parent().parent().append(
        '<span class="help-block">' + response.errors.value + '</span>'
      );
    }
  }

  var _hideValidationErrors = function() {
    $('#modifyMembersForm .has-error').removeClass('has-error');
    $('#modifyMembersForm .help-block').remove();
  }

  return {
    init: init,
    show: show,
    update: update
  }
})();
