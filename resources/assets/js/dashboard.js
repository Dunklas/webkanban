/* Variable where boards owned by current user are stored */
var myBoards;

/* Variable where boards shared with current user are stored */
var sharedBoards;

$("#document").ready(function() {
  registerHandlers();
  resetBoardCards();

  /* Initialize CreateBoard-dialogue with callback functions */
  CreateBoard.init(
    function() {
      // Board created successfully, re-render this view.
      resetBoardCards();
    },
    function() {
      // Error occurred while creating board
      Error.setMessage('An error occurred while creating your board. Please try again.');
      Error.show();
    }
  )

  /* Check every 5s if there is new data (shared boards). */
  setInterval(function() {
    resetBoardCards();
  }, 5000);
});

/* Fires getMyBoards and getSharedBoards, compare result with previous data.
 * If not equals, re-create views. */
function resetBoardCards() {
  getMyBoards(function(response) {
    /* Only re-render view if we find a difference between response and myBoards */
    if (response != myBoards) {
      myBoards = response;
      /* Skip first div, since this one is used as a "create button" */
      $('#myBoardsContainer').children().not('div:first').remove();
      renderBoard('#myBoardsContainer', JSON.parse(myBoards));
    }
  });
  getSharedBoards(function(response) {
    /* Only re-render view if we find a difference between response and sharedBoards */
    if (response != sharedBoards) {
      sharedBoards = response;
      $('#sharedBoardsContainer').empty();
      renderBoard('#sharedBoardsContainer', JSON.parse(sharedBoards));
    }
  });
}

/* Fetches boards owned by current user (GET) */
function getMyBoards(onSuccess) {
  var response = $.get("/owned_boards", function(response) {
    onSuccess(response);
  }).fail(function() {
    Error.setMessage('An error occurred while loading your boards. Please try again later.');
    Error.show();
  });
}

/* Fetches boards shared with current user (GET) */
function getSharedBoards(onSuccess) {
  var response = $.get("/shared_boards", function(response) {
    onSuccess(response);
  }).fail(function() {
    Error.setMessage('An error occurred while loading your boards. Please try again later.');
    Error.show();
  });
}

/* Renders board data in this view.
 * Hidden input element is used to store id of each board.
 * The id is used to build urls in order to open the boards */
function renderBoard(parent, data) {
  var boardHTML = "";
  $.each(data, function(index, obj) {
    boardHTML += '<div class="card card-postit board-link">';
    boardHTML += '<input type="hidden" value="' + obj.id + '">';
    boardHTML += '<span class="card-title">' + obj.name + '</span>';
    boardHTML += '</div>';
  });
  $(parent).append(boardHTML);
}

/* Setup handlers for click events */
function registerHandlers() {
  /* When create-board-card is clicked, open modal. */
  $('#createBoardCard').on('click', function() {
    CreateBoard.show();
  });

  /* Handler for all other cards (except the create-board-one). Uses event delegation
   * since the elements (.board-card) doesn't exist until fetched from server.
   * Each element has a hidden input field with a board-id as first element, which is used
   * to get an id for the link. */
  $('.card-container').on('click', '.board-link', function() {
    var id = $(this).find('>:first-child').val();
    window.location.href = 'viewboard/' + id;
  });
}
