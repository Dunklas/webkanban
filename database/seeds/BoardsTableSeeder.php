<?php

use Illuminate\Database\Seeder;

/*
 * Database seed for boards-table.
 * Inserts data to be used for test purposes.
 */
class BoardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

      DB::table('boards')->insert([
        'name' => 'Dunklas private',
        'owner_id' => '1',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('boards')->insert([
        'name' => 'Dunklas shared',
        'owner_id' => '1',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('boards')->insert([
        'name' => 'Frida private',
        'owner_id' => '2',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('boards')->insert([
        'name' => 'Frida shared',
        'owner_id' => '2',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('boards')->insert([
        'name' => 'Hövet private',
        'owner_id' => '3',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('boards')->insert([
        'name' => 'Hövet shared',
        'owner_id' => '3',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);


      DB::table('board_user')->insert([
        'board_id' => '2',
        'user_id' => '2',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('board_user')->insert([
        'board_id' => '2',
        'user_id' => '3',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('board_user')->insert([
        'board_id' => '4',
        'user_id' => '1',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('board_user')->insert([
        'board_id' => '4',
        'user_id' => '3',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('board_user')->insert([
        'board_id' => '6',
        'user_id' => '1',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('board_user')->insert([
        'board_id' => '6',
        'user_id' => '2',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

    }
}
