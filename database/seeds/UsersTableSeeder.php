<?php

use Illuminate\Database\Seeder;

/*
 * Database seed for users-table.
 * Inserts data to be used for test purposes.
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

      DB::table('users')->insert([
        'email' => 'dunklas@gmail.com',
        'password' => Hash::make('123456'),
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('users')->insert([
        'email' => 'frida.persson@outlook.com',
        'password' => Hash::make('123456'),
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('users')->insert([
        'email' => 'mohede@gmail.com',
        'password' => Hash::make('123456'),
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
    }
}
