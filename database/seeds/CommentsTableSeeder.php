<?php

use Illuminate\Database\Seeder;

/*
 * Database seed for comments-table.
 * Inserts data to be used for test purposes.
 */
class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

      DB::table('comments')->insert([
        'message' => 'Cut the tomatoes.',
        'user_id' => '1',
        'task_id' => '2',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('comments')->insert([
        'message' => 'Fried the burgers.',
        'user_id' => '2',
        'task_id' => '2',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);

      DB::table('comments')->insert([
        'message' => 'Served the dinner!',
        'user_id' => '3',
        'task_id' => '2',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
    }

}
