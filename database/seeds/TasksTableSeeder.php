<?php

use Illuminate\Database\Seeder;

/*
 * Database seed for tasks-table.
 * Inserts data to be used for test purposes.
 */
class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      DB::table('tasks')->insert([
        'name' => 'Fix board view',
        'status' => 'todo',
        'board_id' => '2',
        'assigned_to' => '1',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('tasks')->insert([
        'name' => 'Make dinner',
        'status' => 'doing',
        'board_id' => '2',
        'assigned_to' => '1',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
      DB::table('tasks')->insert([
        'name' => 'Setup middleware',
        'status' => 'done',
        'board_id' => '2',
        'assigned_to' => '1',
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")
      ]);
    }
}
