<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
 * Migration for tasks table in database.
 */
class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Create table for tasks, including name/title, an enum for status of the task, and foreign keys
         * referencing fields in boards (the board which a task belong to) and users (a user assigned to a task) */
        Schema::create('tasks', function (Blueprint $table) {
            $table->string('name');
            $table->enum('status', ['todo', 'doing', 'done', 'archived']);
            $table->integer('board_id')->unsigned();
            $table->integer('assigned_to')->unsigned()->nullable();
            $table->increments('id');
            $table->timestamps();

            $table->foreign('board_id')
              ->references('id')->on('boards')
              ->onDelete('cascade');
            $table->foreign('assigned_to')
              ->references('id')->on('users')
              ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
