<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
 * Migration for comments table in database.
 */
class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        /* Creates table for comments, including field for the actual message, and foreign keys
         * referencing fields in users (author of a comment) and tasks (the task which a comment belong to) */
        Schema::create('comments', function (Blueprint $table) {
            $table->string('message');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('task_id')->unsigned();
            $table->increments('id');
            $table->timestamps();

            $table->foreign('task_id')
              ->references('id')->on('tasks')
              ->onDelete('cascade');
            $table->foreign('user_id')
              ->references('id')->on('users')
              ->onDelete('set null'); // Maybe should have a default user instead? Such as 'Deleted user'?
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('comments');
    }
}
