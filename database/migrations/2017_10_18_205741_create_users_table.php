<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
 * Migration for users table in database.
 */
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Enable foreign key constraints */
        Schema::enableForeignKeyConstraints();

        /* Create user table with id, email and password field.
         * rememberToken() includes a remember_token field which may be used for "remember me"-sessions.
         * timestamps() includes created_at and updated_at fields. */
        Schema::create('users', function (Blueprint $table) {
          $table->increments('id');
          $table->string('email')->unique();
          $table->string('password');
          $table->rememberToken();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::disableForeignKeyConstraints();
    }
}
