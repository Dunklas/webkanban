<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
 * Migration for board_user table in database.
 * Used for many to many relation between boards and users-tables.
 */
class CreateBoarduserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      
        /* Create table with foreign keys referencing id-fields in boards- and users-table */
        Schema::create('board_user', function (Blueprint $table) {
          $table->integer('board_id')->unsigned();
          $table->integer('user_id')->unsigned();
          $table->timestamps();

          $table->foreign('board_id')
            ->references('id')->on('boards')
            ->onDelete('cascade');
          $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boarduser');
    }
}
