let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .scripts([
       'resources/assets/js/util.js',
       'resources/assets/js/common.js'
   ], 'public/js/all.js')
   .scripts([
       'resources/assets/js/dialogues/create_board.js',
       'resources/assets/js/dialogues/error.js',
       'resources/assets/js/dashboard.js'
   ], 'public/js/dashboard.js')
   .scripts([
       'resources/assets/js/dialogues/modify_task.js',
       'resources/assets/js/dialogues/create_task.js',
       'resources/assets/js/dialogues/modify_members.js',
       'resources/assets/js/dialogues/modify_board.js',
       'resources/assets/js/dialogues/archived_tasks.js',
       'resources/assets/js/dialogues/error.js',
       'resources/assets/js/board.js'
   ], 'public/js/board.js');
